# C2Games Hasura

Hasura Metadata used for C2Games Hasura Servers.

## Production Usage

### Hasura Cloud

Use the hasura console container `registry.gitlab.com/c2-games/scoring/hasura/hasura-console:latest`
and the configuration file in `<repo-root>/hasura/config.yaml` to apply the
metadata and migrations in this repository to a cloud instance.

```sh
docker-compose exec hasura_console hasura migrate apply --database-name default
docker-compose exec hasura_console hasura metadata apply
```

### Docker

The git-deploy container that is built by this repository is likely the fastest
way to get up an running.

These secrets should be stored in an env file, such as `hasura.env`

```sh
HASURA_GRAPHQL_ADMIN_SECRET=myadminsecretkey
HASURA_GRAPHQL_DATABASE_URL="postgres://postgres:SuperStrongSecret@hasura_postgres:5432/postgres"
```

The following `docker-compose.yml` would start up an instance of hasura, provided
`keycloak`, `keycloak_graphql` and `hasura_postgres` were all accessible.

```yml
  hasura:
    image: registry.gitlab.com/c2-games/scoring/hasura/hasura-deploy-git:latest
    restart: always
    env_file:
      - ./hasura.env
    environment:
      HASURA_GIT_REPO_URL: "https://gitlab.com/c2-games/scoring/hasura.git"
      HASURA_GIT_REFSPEC: "main"
      HASURA_GIT_METADATA_DIR: "hasura/metadata"
      HASURA_GIT_MIGRATIONS_DIR: "hasura/migrations"
      HASURA_GRAPHQL_ENABLE_REMOTE_SCHEMA_PERMISSIONS: "true"
      HASURA_GRAPHQL_JWT_SECRET: '{ "allowed_skew": 600, "jwk_url": "https://auth.c2games.org/realms/dev/protocol/openid-connect/certs" }'
      HASURA_GRAPHQL_ENABLE_CONSOLE: "true"
      HASURA_GRAPHQL_DEV_MODE: "true"
      HASURA_GRAPHQL_LOG_LEVEL: "warn"
      HASURA_GRAPHQL_ENABLED_LOG_TYPES: "startup, http-log, webhook-log, websocket-log, query-log"
      HASURA_GRAPHQL_UNAUTHORIZED_ROLE: anonymous
      HASURA_GRAPHQL_ENABLE_TELEMETRY: "false"
      KEYCLOAK_GRAPHQL_URL: http://keycloak_graphql:8000
    ports:
      - "8080:8080"
```

## Development Usage

Clone this repository, then start the containers on the `c2games` docker network
by running `docker compose up`, it may take a few minutes for all containers to
become ready, be patient when first provisioning a new instance.

Access the [hasura console](http://localhost:9090). Any modifications to
metadata, schema or tables will become available in the `hasura` directory.

**Note:** The following log message is not an error and is purely informational:

* ```json
  {"detail":{"error":null,"message":"refreshing JWK from endpoint: https://auth.c2games.org/realms/dev/protocol/openid-connect/certs"},"level":"info","timestamp":"2022-10-03T21:35:02.362+0000","type":"jwk-refresh-log"}
  ```

### Running Hasura Console Locally

**A special note for Windows/Mac Users:** Host Docker Networking doesn't work on some platforms. Hasura CLI should be
installed & run locally using the following two steps.

If you experience issues with Docker host networking, it's recommended to comment out the hasura_console in the
`docker-compose.yml` file and run the hasura console locally.

#### Installing Hasura CLI

See https://hasura.io/docs/latest/hasura-cli/install-hasura-cli/ for more information and installation options.

One possible option is utilizing NPM:

```bash
npm i -g hasura-cli
```

#### Running Hasura Console

Run the following command from the `hasura` directory within the repo (**NOT** the top level of the repo):

```bash
hasura console
```

### **Valid**ation H**asaur**a

Validasaur is a microservice that provides [input validation](https://hasura.io/docs/latest/schema/postgres/input-validations/)
for complex checks within Hasura.

The API Docs can be viewed at http://localhost:9091/docs

#### Configuration

Make a file at `validasaur/.env` to contain your configuration. **Note:** some
of these are defined in the `docker-compose.yml` file.

```sh
# JWK_URL points validasaur at the same authentication service as hasura for token validation
VALIDASAUR_AUTH_JWK_URL=https://auth.c2games.org/realms/dev/protocol/openid-connect/certs
# HASURA_ADMIN_SECRET is the admin secret configured for hasura
# in the future client creds will be used
VALIDASAUR_AUTH_HASURA_ADMIN_SECRET=myadminsecretkey
# HASURA_URL is the graphql endpoint of the hasura instance used to look up
# information for validating query/mutation inputs
VALIDASAUR_HASURA_URL=http://localhost:8080/v1/graphql
```

### Interfacing with keycloak

Keycloak is a fundamental component in the authentication and authorization of
this hasura project. Another OIDC provider can be used, but there would likely
be bugs and caveats due to additional tooling built around this integration.

#### User information bridge

A keycloak remote schema (GraphQL) was setup to provide this hasura instance
with information on users based on their keycloak UUID. This requires that JWT
authentication is used (it does not respect the hasura admin secret) and that
HTTP request headers are forwarded to the remote schema. The remote schema will
censor user information if the information is not requested by staff role or by the user that owns that information.

Setup the Keycloak remote schema credentials in the env file used by the `docker-compose.yml`

```sh
echo "KCGQL_KC_CLIENT_SECRET=SecretSecretSecretSecret" >> keycloak_graphql.env
```

Make sure that JWTs for users are provisioned with the correct hasura client
roles and there is an `x-hasura-user-id` mapper to the user's keycloak uuid in
the token.

### (Deprecated) Updating hasura metadata

**Note:** If all modifications are made through http://127.0.0.1:9090 all
metadata changes will be auto-exported to the `hasura` directory.

A docker container with the hasura cli binary has been provided for ease of
use in the docker-compose. Run the script provided in `utils/update-hasura-metadata.sh`.
Now check `git status` to ensure the metadata changes are tracked, commit the
changes.

### (Deprecated) Creating or updating a table or schema

**Note:** If all modifications are made through http://127.0.0.1:9090 all
metadata changes will be auto-exported to the `hasura` directory.

When creating or updating tables and schemas, create a new migration file and
delete the old migration. Run the script provided in
`utils/create-hasura-migration.sh` to create a new migration file with the
changes, add the new files to git, then remove the old ones. **Note:** If a
migration is intended to be incremental, this will need a new script created to
generate an additional migration file with an `up.sql` and a `down.sql` so the
migration can be rolled back if needed. For now, this ability is not provided as
it does not have a use-case yet.

### Using the hasura cli

The `hasura_console` container in the `docker-compose` runs indefinitely,
simply start a `bash` shell and use the hasura binary in the container to do
any required actions.

```sh
$ docker-compose exec hasura_console bash
root@03d1d1906e6b:/hasura# hasura --help
```

### Using seed data

#### Load all shipped seed datasets

Below is an example using the `dev` seed dataset.

```sh
utils/apply-hasura-seed.sh dev
```

#### Create a new seed dataset

Below is an example creating the `dev` seed dataset.

```sh
utils/create-hasura-seed.sh dev
```

Add the new dataset and delete any old or conflicting datasets to/from git.

### Pushing Images

To push generated images to the registry:

```bash
docker login registry.gitlab.com
docker-compose push
```
