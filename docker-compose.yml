networks:
  default:
    name: c2games

services:
  # Comment out these hasura sections if there is an external instance
  # Hasura
  hasura:
    # Use this image for development with auto-migrations
    image: hasura/graphql-engine:latest.cli-migrations-v3
    # Use this image for no auto-migrations
    # image: hasura/graphql-engine:latest
    restart: always
    depends_on:
      - hasura_postgres
      - keycloak_graphql
    healthcheck:
      test: ["CMD", "curl", "-f", "http://localhost:8080/healthz"]
      interval: 5s
      timeout: 3s
      retries: 12
      start_period: 10s
    environment:
      HASURA_GRAPHQL_ENABLE_REMOTE_SCHEMA_PERMISSIONS: "false"
      HASURA_GRAPHQL_JWT_SECRET: '{ "allowed_skew": 600, "jwk_url": "https://auth.ncaecybergames.org/realms/dev/protocol/openid-connect/certs" }'
      HASURA_GRAPHQL_DATABASE_URL: "postgres://postgres:postgrespassword@hasura_postgres:5432/postgres"
      HASURA_GRAPHQL_ENABLE_CONSOLE: "false"
      HASURA_GRAPHQL_DEV_MODE: "true"
      HASURA_GRAPHQL_LOG_LEVEL: "warn"
      HASURA_GRAPHQL_ENABLED_LOG_TYPES: "startup, http-log, webhook-log, websocket-log, query-log"
      HASURA_GRAPHQL_ADMIN_SECRET: myadminsecretkey
      HASURA_GRAPHQL_UNAUTHORIZED_ROLE: anonymous
      HASURA_GRAPHQL_ENABLE_TELEMETRY: "false"
      HASURA_GRAPHQL_MIGRATIONS_SERVER_TIMEOUT: "10"
      KEYCLOAK_GRAPHQL_URL: http://keycloak_graphql:8000
      VALIDATION_URL: http://validasaur:9091
      # uncomment the following to use validasaur locally instead of within docker
#      VALIDATION_URL: http://host.docker.internal:9092
#    extra_hosts:
#      - "host.docker.internal:host-gateway"
    ports:
      - "8080:8080"
    volumes:
      # These are only needed for auto-migrations
      - ./hasura/migrations:/hasura-migrations
      - ./hasura/metadata:/hasura-metadata

  hasura_console:
    image: registry.gitlab.com/c2-games/scoring/hasura/hasura-console
    restart: on-failure
    build:
      context: .
      dockerfile: Dockerfile.HasuraConsole
    command: [
      "hasura", "console", "--no-browser",
      "--api-port", "8081",
      "--console-port", "9090"
    ]
    depends_on:
      hasura:
        condition: service_healthy
    # this has to use host networking because the cli tool is intended
    # to be run locally on your own system and not within a container.
    # this will hopefully be fixed in the future.
    # refs https://github.com/hasura/graphql-engine/pull/9396
    # refs https://github.com/hasura/graphql-engine/pull/9800
    network_mode: host
    volumes:
      - ./hasura:/hasura:rw

  # PostgreSQL Server for Hasura
  hasura_postgres:
    image: postgres:14-alpine
    restart: always
    volumes:
      - hasura_postgres_data:/var/lib/postgresql/data
      - ./postgres/initdb.d/:/docker-entrypoint-initdb.d/
    environment:
      POSTGRES_PASSWORD: postgrespassword

  hasura_storage:
    image: nhost/hasura-storage:0.6.0
    restart: unless-stopped
    ports:
      - '8181:8000'
    environment:
      DEBUG: "true"
      HASURA_ENDPOINT: http://hasura:8080/v1
      HASURA_GRAPHQL_ADMIN_SECRET: "myadminsecretkey"
      S3_ENDPOINT: http://s3_bucket:4566
      S3_ACCESS_KEY: "testkey"
      S3_SECRET_KEY: "testsecret"
      S3_REGION: "us-east-1"
      # This works for R2 buckets
      # S3_REGION: "auto"
      S3_BUCKET: "default"
    command: serve
    depends_on:
      - hasura
      - s3_bucket

  s3_bucket:
    image: gresau/localstack-persist:3
    ports:
      - "4566-4599:4566-4599"
    environment:
      - SERVICES=s3  # we only need s3 bucket
      - DEBUG=1
      - AWS_ACCESS_KEY_ID=testkey
      - AWS_SECRET_ACCESS_KEY=testsecret
    volumes:
      - ./localstack/init/ready.d:/etc/localstack/init/ready.d
      - localstack_data:/persisted-data

  validasaur:
    build:
      context: ./validasaur
    # run the default dev server
    entrypoint:
      - poetry
      - run
      - dev
    command: []
    healthcheck:
      test: ["CMD", "curl", "-f", "http://localhost:9091/v1/readyz"]
      interval: 5s
      timeout: 3s
      retries: 3
    environment:
      VALIDASAUR_AUTH_JWK_URL: https://auth.ncaecybergames.org/realms/dev/protocol/openid-connect/certs
      VALIDASAUR_HASURA_URL: http://hasura:8080/v1/graphql
      VALIDASAUR_AUTH_HASURA_ADMIN_SECRET: myadminsecretkey
    # Uncomment the following to store real secrets in an untracked file
    # env_file:
    #   # define VALIDASAUR_AUTH_CLIENT_SECRET
    #   # or VALIDASAUR_AUTH_HASURA_ADMIN_SECRET here
    #   - ./validasaur/.env
    volumes:
      - ./validasaur/validasaur:/src/validasaur
    ports:
      - "9091:9091"

  keycloak_graphql:
    image: registry.gitlab.com/c2-games/infrastructure/auth/keycloak-graphql:latest
    restart: always
    # uncomment for direct access to the graphql playground
    # ports:
    #   - 8000:8000
    env_file:
      - ./keycloak_graphql.env
    environment:
      - KCGQL_KC_CLIENT_ID=federation
      # - KCGQL_KC_CLIENT_SECRET=SecretSecretSecretSecret  # Put this in your env file
      - KCGQL_KC_REALM=dev
      - KCGQL_KC_SERVER=https://auth.ncaecybergames.org
      - KCGQL_DEBUG=true
      - KCGQL_INTROSPECTION=true
      - KCGQL_CACHE_LIFETIME=5m
      - KCGQL_CENSOR_FRONT=1
      - KCGQL_CENSOR_BACK=1
      - KCGQL_GQL_URL=http://hasura:8080/v1/graphql
      - KCGQL_BADGR_USERNAME=sradigan+badgrtest@c2games.org
      # - KCGQL_BADGR_PASSWORD="asdf"  # Put this in your env file
      - KCGQL_BADGR_ISSUER_ID=-FSGvAdTSOKyqVvnDMN__A
      - KCGQL_BADGR_API_ROOT_PUBLIC=https://api.test.badgr.com/public
      - KCGQL_BADGR_API_ROOT_AUTHENTICATED=https://api.test.badgr.com/v2
      - KCGQL_BADGR_CACHE_LIFETIME=30s

volumes:
  hasura_postgres_data:
  localstack_data:
