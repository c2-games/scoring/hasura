#!/usr/bin/env bash
set -x
# Make the default storage bucket
awslocal s3 mb s3://default
set +x