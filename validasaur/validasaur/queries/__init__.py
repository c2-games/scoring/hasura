import json
from typing import Dict, List, Optional, Tuple
from uuid import UUID

from gql import gql
from pydantic import BaseModel

from validasaur.db import get_client
from validasaur.models.event import EventRegistration
from validasaur.models.exceptions import ValidationFailed
from validasaur.models.team import Team

team_user_query = gql(
    """
    query GetTeamInstitutionsAndUserInfo($team_id: uuid!, $user_id: ID!) {
        teams_by_pk(id: $team_id) {
            institution {
                domains {
                    domain
                }
            }
        }
        c2games_user_by_pk(id: $user_id) {
            email
        }
    }"""
)


async def get_team_institution_domains_with_user_info(team_id: str, user_id: str) -> Tuple[str, List[str]]:
    """
    Get the email address for the user ID, and all institution domains for the team ID.
    """
    async with await get_client() as client:
        result = await client.execute(team_user_query, variable_values={"team_id": team_id, "user_id": user_id})
        team_data = result["teams_by_pk"]
        user_data = result["c2games_user_by_pk"]
        if not team_data:
            raise ValidationFailed(f"team not found: {team_id}")
        if not user_data:
            raise ValidationFailed(f"user not found: {user_id}")
        user_email = user_data["email"]

        team_domains = []
        for entry in team_data.get("institution", {}).get("domains", []):
            domain = entry.get("domain")
            if domain:
                team_domains.append(domain)

        return user_email, team_domains


team_event_query = gql(
    """
    query GetTeamWithEventInfo($team_id: uuid!) {
        teams_by_pk(id: $team_id) {
            id
            members {
                user_id
            }
            name
            long_name
            team_lead_id
            liaison_id
            events {
                event_id
                event_registration_setting {
                    max_team_size
                    min_team_size
                    require_liaison
                    region_id
                    registration_active
                    registration_close
                    registration_open
                    roster_change_close
                }
                event {
                    id
                    name
                    started
                    ended
                    active
                }
            }
        }
    }"""
)


async def get_team_with_events_by_team_id(team_id: str) -> Tuple[Team, List[EventRegistration]]:
    """
    Return a team and all events that team is registered for.
    """
    async with await get_client() as client:
        result = await client.execute(team_event_query, variable_values={"team_id": team_id})
        data = result["teams_by_pk"]

        if not data:
            raise ValidationFailed(f"team not found: {team_id}")
        print(json.dumps(data, indent=2))
        return Team(**data), [EventRegistration(**e) for e in data["events"]]


team_liaison_query = gql(
    """
    query GetTeamLiaison($team_id: uuid!) {
        teams_by_pk(id: $team_id) {
            liaison_id
        }
    }"""
)


async def get_team_liaison(team_id: str) -> Optional[UUID]:
    async with await get_client() as client:
        result = await client.execute(team_liaison_query, variable_values={"team_id": team_id})
        return result["teams_by_pk"]["liaison_id"]


team_lead_query = gql(
    """
    query GetTeamLead($team_id: uuid!) {
        teams_by_pk(id: $team_id) {
            team_lead_id
        }
    }"""
)


async def get_team_lead(team_id: str) -> Optional[UUID]:
    async with await get_client() as client:
        result = await client.execute(team_lead_query, variable_values={"team_id": team_id})
        return result["teams_by_pk"]["team_lead_id"]


team_members_query = gql(
    """
    query GetTeamMembers($team_id: uuid!) {
        teams_by_pk(id: $team_id) {
            members {
                user_id
            }
        }
    }"""
)


async def get_team_members(team_id: str) -> List[Dict[str, str]]:
    async with await get_client() as client:
        result = await client.execute(team_members_query, variable_values={"team_id": team_id})
        return result["teams_by_pk"]["members"]


event_registration_checks_query = gql(
    """
    query GetTeamRegistrationValidationData($team_id: uuid!, $event_id: uuid!) {
        events_by_pk(id: $event_id) {
            event_group {
                max_teams_per_event
                institution_teams_same_event
                max_institution_teams_per_event
                events {
                    id
                    teams {
                        team {
                            institution_id
                        }
                    }
                }
            }
            event_registration_setting {
                region_id
                registration_active
                max_team_size
                min_team_size
                require_liaison
            }
            teams {
                team_id
            }
        }
        teams_by_pk(id: $team_id) {
            liaison_id
            institution_id
            institution {
                regions {
                    region_id
                }
            }
            team_size
        }
    }"""
)


# This is only added for use below. Using this cuts down on duplicate checks
# by just checking if the parent even allocated this
class EventGroupSettings(BaseModel):
    max_teams_per_event: Optional[int]
    institution_teams_same_event: bool
    max_institution_teams_per_event: Optional[int]
    # data structure is: { event_id: {institution_id, registration_count} }
    institution_registrations: Optional[Dict[UUID, Dict[UUID, int]]]


# This doesn't reflect anything in the database, it just makes a tidier return value
class EventRegistrationValidationData(BaseModel):
    event_registration_active: bool
    event_max_team_size: Optional[int] = None
    event_min_team_size: Optional[int] = None
    event_region_id: Optional[UUID] = None
    event_teams: int
    event_group_settings: Optional[EventGroupSettings] = None
    team_region_ids: List[UUID]
    team_institution_id: Optional[UUID] = None
    team_size: int
    team_liaison_id: Optional[UUID] = None
    require_liaison: bool


def _get_event_settings(event_data: Dict) -> Optional[EventGroupSettings]:
    if not event_data.get("event_group"):
        return None
    # Get any event group settings
    settings = event_data.get("event_group", {})
    # Summarize the count of institutions registered to each event
    # in this event group
    institution_registrations: Dict[UUID, Dict[UUID, int]] = {}
    for event in settings.get("events", []):
        eid = event["id"]
        registrations: Dict[UUID, int] = {}
        for team in event.get("teams", []):
            tid = team["team"]["institution_id"]
            if tid:
                # If this team has an institution, add it to the list
                v = registrations.get(tid, 0)
                registrations[tid] = v + 1
        # If there were teams with an institution found, add them
        if registrations:
            institution_registrations[eid] = registrations

    return EventGroupSettings(
        max_teams_per_event=settings["max_teams_per_event"],
        institution_teams_same_event=settings["institution_teams_same_event"],
        max_institution_teams_per_event=settings["max_institution_teams_per_event"],
        institution_registrations=institution_registrations or None,
    )


async def get_event_registration_validation_data(
    team_id: str,
    event_id: str,
) -> EventRegistrationValidationData:
    async with await get_client() as client:
        result = await client.execute(
            event_registration_checks_query, variable_values={"team_id": team_id, "event_id": event_id}
        )
        team_data = result["teams_by_pk"]
        event_data = result["events_by_pk"]

        if not team_data:
            raise ValidationFailed(f"team not found: {team_id}")
        if not event_data:
            raise ValidationFailed(f"event not found: {event_id}")
        # Find the team region id, for checking region restrictions
        team_region_ids = []
        institution = team_data.get("institution") or {}
        for i in institution.get("regions", []):
            rid = i.get("region_id")
            if rid:
                team_region_ids.append(rid)
        event_group_settings = _get_event_settings(event_data)
        return EventRegistrationValidationData(
            event_registration_active=event_data["event_registration_setting"]["registration_active"],
            event_max_team_size=event_data["event_registration_setting"]["max_team_size"],
            event_min_team_size=event_data["event_registration_setting"]["min_team_size"],
            team_region_ids=team_region_ids,
            team_institution_id=team_data.get("institution_id"),
            team_size=team_data["team_size"],
            event_region_id=event_data.get("event_registration_setting", {}).get("region_id"),
            event_teams=len(event_data.get("teams", [])),
            event_group_settings=event_group_settings,
            team_liaison_id=team_data.get("liaison_id"),
            require_liaison=event_data["event_registration_setting"]["require_liaison"],
        )
