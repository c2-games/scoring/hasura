import json
from typing import Dict, Tuple

import jwt
import requests
from starlette.requests import Request

from validasaur.config import config
from validasaur.models.exceptions import ValidationFailed

_JWK_CACHE: Dict[str, Dict[str, str]] = {}


def auth_ready() -> Tuple[bool, str]:
    """Test if auth is ready."""
    try:
        ret = get_public_keys(config.auth.jwk_url)
        if not ret:
            return (False, "could not retrieve public keys")
    except:  # pylint: disable=bare-except
        return (False, "could not retrieve public keys")
    return (True, "ok")


def get_public_keys(url: str) -> Dict:
    """Get the auth.c2games.org public keys to decode JWTs"""
    jwks = requests.get(url, timeout=5).json()
    pub_keys = {}
    for jwk in jwks["keys"]:
        kid = jwk["kid"]
        pub_keys[kid] = jwt.algorithms.RSAAlgorithm.from_jwk(json.dumps(jwk))

    return pub_keys


def get_token(authorization: str | None) -> str:
    """Get the JWT from authorization header"""
    if not authorization:
        raise ValidationFailed("INTERNAL SERVER ERROR: No Authorization header provided")
    if not authorization.startswith("Bearer "):
        raise ValidationFailed("INTERNAL SERVER ERROR: Invalid Authorization header")
    return authorization[7:]


def verify_token(req: Request) -> Dict:
    token = get_token(req.headers.get("authorization"))

    if not _JWK_CACHE.get(config.auth.jwk_url):
        _JWK_CACHE[config.auth.jwk_url] = get_public_keys(config.auth.jwk_url)

    try:
        kid = jwt.get_unverified_header(token)["kid"]
        decoded_token = jwt.decode(
            token,
            _JWK_CACHE[config.auth.jwk_url][kid],
            algorithms=["RS256"],
            options={"verify_signature": True, "verify_aud": False, "verify_exp": True},
        )

        print("jwt validated!")
        return decoded_token
    except Exception as e:
        print(e)
        # We don't want to raise this an preserve the e as it might contain information
        # that shouldn't be exposed via GQL errors
        raise ValidationFailed("INTERNAL SERVER ERROR: INVALID TOKEN")  # pylint: disable=W0707
