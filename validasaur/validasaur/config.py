from cincoconfig import Schema
from cincoconfig.fields import SecureField, UrlField

schema = Schema(env="VALIDASAUR")

schema.auth.jwk_url = UrlField(
    default="https://auth.c2games.org/realms/dev/protocol/openid-connect/certs", required=True
)
# schema.auth.url = UrlField(required=True)
# schema.auth.client_id = StringField(default="validasaur", required=True)
# schema.auth.client_secret = SecureField(required=True)
schema.auth.hasura_admin_secret = SecureField(required=True)

schema.hasura.url = UrlField(required=True)

config = schema()

if __name__ == "__main__":
    config.validate()
    print(config.dumps(format="json", pretty=True, sensitive_mask=None).decode())
