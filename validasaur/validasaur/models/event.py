from datetime import datetime
from typing import Optional
from uuid import UUID

from pydantic import BaseModel


class EventRegistrationSetting(BaseModel):
    max_team_size: int
    min_team_size: int
    require_liaison: bool
    region_id: Optional[UUID] = None
    registration_active: bool
    registration_close: Optional[datetime] = None
    registration_open: Optional[datetime] = None
    roster_change_close: Optional[datetime] = None


class Event(BaseModel):
    id: UUID
    name: str
    start: Optional[datetime] = None
    end: Optional[datetime] = None
    started: Optional[bool] = None
    ended: Optional[bool] = None
    active: Optional[bool] = None


class EventRegistration(BaseModel):
    event_id: UUID
    event_registration_setting: EventRegistrationSetting
    event: Event


class EventGroup(BaseModel):
    id: UUID
    name: str
    max_teams_per_event: int
    institution_teams_same_event: bool
    max_institution_teams_per_event: int
