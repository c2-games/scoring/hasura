from typing import Dict, List, Optional
from uuid import UUID

from pydantic import BaseModel


class Team(BaseModel):
    id: UUID
    name: str
    long_name: Optional[str] = None
    liaison_id: Optional[str] = None
    regions: Optional[Dict] = None
    members: Optional[List[Dict]] = None
