from enum import Enum
from typing import Dict, List

from pydantic import BaseModel


class UserRole(Enum):
    # These names need to match the database, so snake_case is desired
    # pylint: disable=C0103
    attacker = "attacker"
    ctf_admin = "ctf_admin"
    event_admin = "event_admin"
    participant = "participant"
    scoringpod = "scoringpod"
    staff = "staff"


class MutationData(BaseModel):
    input: List[Dict[str, str | Dict]]
    pk_columns: List[Dict[str, str]] | None = None


class HasuraWebhookData(BaseModel):
    version: int
    role: UserRole
    session_variables: Dict[str, str]
    data: MutationData
