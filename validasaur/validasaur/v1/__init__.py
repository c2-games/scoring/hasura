from fastapi import APIRouter

from . import registration, status

router = APIRouter()
router.include_router(status.router)
router.include_router(registration.router)
