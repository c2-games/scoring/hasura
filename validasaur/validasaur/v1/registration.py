from datetime import datetime
from uuid import UUID

from fastapi import APIRouter, Depends

from validasaur.auth import verify_token
from validasaur.models import HasuraWebhookData
from validasaur.models.event import EventRegistration
from validasaur.models.exceptions import ValidationFailed
from validasaur.models.team import Team
from validasaur.queries import (
    get_event_registration_validation_data,
    get_team_institution_domains_with_user_info,
    get_team_lead,
    get_team_liaison,
    get_team_members,
    get_team_with_events_by_team_id,
)

router = APIRouter(prefix="/registration", dependencies=[Depends(verify_token)])


def can_modify_roster_for_event(team: Team, event: EventRegistration, member_count_offset: int = 0) -> None:
    now = datetime.now().timestamp()
    # Note: This might be implicit that if a team is registered for an event
    # then registration must have been open at some point. With that in mind,
    # changes should only be allowed until roster_change_close and not after.

    # Changes are only made before roster change close
    if not event.event_registration_setting.roster_change_close:
        raise ValidationFailed(f"roster changes are not permitted for {event.event.name}")
    if now > event.event_registration_setting.roster_change_close.timestamp():
        raise ValidationFailed(f"roster changes are no longer permitted for {event.event.name}")
    # Changes can't be made after an event has ended. This is to preserve
    # historical data, if we allow roster changes, then we don't really know
    # who participated in that event.
    # Note: This likely is already handled by the roster_change_close check,
    # it is unlikely that that time would be after the event closure. In the
    # event that the handling of roster_change_close == null changes, this
    # will preserve changes against teams who participated in prior events
    if event.event.ended:
        raise ValidationFailed(f"event {event.event.name} has ended, team can not be modified")

    # The updated team size must be the maximum or less
    if team.members and len(team.members) + member_count_offset > event.event_registration_setting.max_team_size:
        raise ValidationFailed(f"team would be too large for event {event.event.name}")
    # The updated team size must be the minimum or more
    if team.members and len(team.members) + member_count_offset < event.event_registration_setting.min_team_size:
        raise ValidationFailed(f"team would be too small for event {event.event.name}")


async def validate_user_in_team_institution(team_id: str, user_id: str) -> None:
    user_email, team_domains = await get_team_institution_domains_with_user_info(team_id, user_id)
    user_domain = user_email.split("@", 1)[1]
    if user_domain not in team_domains:
        raise ValidationFailed(f"user email is not included in institution domains: {', '.join(team_domains)}")
    return


async def validate_user_not_team_liaison(team_id: str, user_id: str) -> None:
    liaison_id = await get_team_liaison(team_id)
    if user_id == liaison_id:
        raise ValidationFailed("the team liaison can not be a team member")
    return


@router.post("/roster_modifications/add", status_code=200)
async def roster_modifications_on_add(data: HasuraWebhookData) -> None:
    print(data)
    checked_teams = []
    for insertion in data.data.input:
        team_id = str(insertion["team_id"])
        user_id = str(insertion["user_id"])
        # Check if user is a part of the team institution
        await validate_user_in_team_institution(team_id, user_id)
        # Check if user is team liaison
        await validate_user_not_team_liaison(team_id, user_id)
        # Don't do the same checks again if multiple members are being
        # added to a team that has already been checked and was validated
        if insertion["team_id"] in checked_teams:
            continue
        # Count up how many new members for this team are being inserted
        new_members = 0
        for i in data.data.input:
            if i["team_id"] == insertion["team_id"]:
                new_members += 1
        # Get all events associated with this team
        team, events = await get_team_with_events_by_team_id(str(insertion["team_id"]))
        for entry in team.members or []:
            if user_id == entry.get("user_id"):
                raise ValidationFailed("user is already on this team")
        if not events:
            print("no events registered, modification allowed")
            continue

        for event in events:
            can_modify_roster_for_event(team, event, member_count_offset=new_members)

        checked_teams.append(insertion["team_id"])

    # No errors raised, so modification is allowed
    return


@router.post("/roster_modifications/delete", status_code=200)
async def roster_modifications_on_delete(data: HasuraWebhookData) -> None:
    print(data)
    for entry in data.data.input:
        # If pk columns is here, this is likely just 1 request and not multiple
        # I'm not sure why input is an array
        data_pk = entry.get("pk_columns")
        if isinstance(data_pk, dict):
            team_id = data_pk["team_id"]
        else:
            # Check for where clause that includes the team_id, if it doesn't
            # just fail for now. Some extra work will need to be done to allow
            # more flexible deletes later

            # Ignore this error for now, mypy wants to handle everything that
            # where could possibly be and that is exhausting.
            team_id = entry.get("where", {}).get("team_id", {}).get("_eq")  # type: ignore [union-attr]
            # TODO: Handle multiple user removal
            # This is a low priority, it isn't going to happen often by participant role
            # and it increases the scope of complexity when checking for multiple teams
            # from multiple institutions registering within the same event group
            user_id = entry.get("where", {}).get("user_id", {}).get("_eq")  # type: ignore [union-attr]
            if not user_id:
                raise ValidationFailed("refusing request, only single registrations are accepted")

        if not team_id:
            raise ValidationFailed("delete requests must include exact team identifier")
        team, events = await get_team_with_events_by_team_id(team_id)
        if not events:
            print("no events registered, modification allowed")
            continue
        for event in events:
            can_modify_roster_for_event(team, event, member_count_offset=-1)

    # No errors raised, so modification is allowed
    return


async def can_team_register_for_event(team_id: str, event_id: str) -> None:
    # Disable too-many-branches, it doesn't make a ton of sense to break this into multiple functions
    # pylint: disable=R0912
    data = await get_event_registration_validation_data(team_id, event_id)
    # Liaison must be allocated for a registration
    if data.require_liaison and not data.team_liaison_id:
        raise ValidationFailed("event registration requires a team liaison to be named")
    # Target event must be open for registration
    if not data.event_registration_active:
        raise ValidationFailed("registration is not available for this event at this time")
    # Check if event region is in the list of team regions
    #   If the event region is null, anyone is allowed to sign up
    if data.event_region_id and data.event_region_id not in data.team_region_ids:
        raise ValidationFailed("regional restriction on this event excludes team region")
    # Check if team minimum size restriction has been met
    if isinstance(data.event_min_team_size, int) and data.team_size < data.event_min_team_size:
        raise ValidationFailed(f"team must be at least {data.event_min_team_size} to register")
    # Check if team maximum size restriction has been met
    if isinstance(data.event_max_team_size, int) and data.team_size > data.event_max_team_size:
        raise ValidationFailed(f"team must be {data.event_max_team_size} or less to register")
    # If there are no event_group_settings, then this event isn't allocated to an event group
    # and thusly is not restricted by additional settings.
    if not data.event_group_settings:
        return
    egs = data.event_group_settings
    # Check if there is room for teams on this event per event_group settings
    if egs.max_teams_per_event:
        if egs.max_teams_per_event < data.event_teams + 1:
            raise ValidationFailed(f"event team maximum {egs.max_teams_per_event} exceeded")
    # Check if there is room for more teams from this institution per event_group settings
    if egs.max_institution_teams_per_event and egs.institution_registrations and data.team_institution_id:
        team_institute_registrations = 0
        for event, values in egs.institution_registrations.items():
            v = values.get(data.team_institution_id)
            if not v:
                continue
            team_institute_registrations += v
            # Check if all teams from an institution must be on the same event
            if egs.institution_teams_same_event:
                # If so: ensure this institution isn't registered for any other events
                if UUID(event_id) != event and v:
                    raise ValidationFailed("all institution teams must be registered for the same event")
        if team_institute_registrations + 1 > egs.max_institution_teams_per_event:
            raise ValidationFailed(f"event institution team maximum {egs.max_institution_teams_per_event} exceeded")


@router.post("/event_registration/add", status_code=200)
async def event_registration_on_add(data: HasuraWebhookData) -> None:
    print(data)

    # TODO: Handle multiple registrations
    # This is a low priority, it isn't going to happen often by participant role
    # and it increases the scope of complexity when checking for multiple teams
    # from multiple institutions registering within the same event group
    if len(data.data.input) > 1:
        raise ValidationFailed("refusing request, only single registrations are accepted")

    for insertion in data.data.input:
        # This isn't actually needed, it is handled in hasura permissions
        # It might help to use it for a more human-readable error message though
        # TODO: Only team_lead_id can register a team for an event

        team_id = insertion.get("team_id")
        event_id = insertion.get("event_id")
        if not (team_id and event_id):
            raise ValidationFailed("malformed request")

        await can_team_register_for_event(str(team_id), str(event_id))


async def can_team_remove_liaison(team_id: str) -> None:
    _, events = await get_team_with_events_by_team_id(team_id)
    for entry in events:
        if entry.event_registration_setting.require_liaison:
            raise ValidationFailed(f"liaison is required by event {entry.event.name}")


async def can_update_team_lead(team_id: str, team_lead_id: str) -> None:
    now = datetime.now().timestamp()
    _, events = await get_team_with_events_by_team_id(team_id)
    for entry in events:
        if not entry.event_registration_setting.roster_change_close:
            raise ValidationFailed(f"roster changes are not permitted for {entry.event.name}")
        if now > entry.event_registration_setting.roster_change_close.timestamp():
            raise ValidationFailed(f"roster changes are no longer permitted for {entry.event.name}")
        if entry.event.ended:
            raise ValidationFailed(f"event {entry.event.name} has ended, team can not be modified")

    members = await get_team_members(team_id)
    is_member = False
    for i in members:
        if i["user_id"] == team_lead_id:
            is_member = True
            break
    if not is_member:
        raise ValidationFailed("team captain must be a team member")


@router.post("/team/update", status_code=200)
async def team_on_update(data: HasuraWebhookData) -> None:
    # Disable too-many-branches, it doesn't make a ton of sense to break this into multiple functions
    # pylint: disable=R0912
    print(data)
    requester_id = data.session_variables.get("x-hasura-user-id")
    if not requester_id:
        raise ValidationFailed("malformed request")
    for entry in data.data.input:
        data_pk = entry.get("pk_columns")
        if isinstance(data_pk, dict):
            team_id = data_pk["id"]
        else:
            raise ValidationFailed("malformed request")
        if not team_id:
            raise ValidationFailed("refusing request, only single team updates are accepted")
        team_lead_id = await get_team_lead(team_id)
        if requester_id != team_lead_id:
            raise ValidationFailed("only team captain can update team")

        updates = entry.get("_set", {})
        if not isinstance(updates, dict):
            raise ValidationFailed("malformed request")

        liaison_id = updates.get("liaison_id")
        if liaison_id:  # This both validates it is here and not null
            # Ensure liaison is from the same institution
            await validate_user_in_team_institution(team_id, liaison_id)
            # Ensure liaison_id is not a member of the team
            members = await get_team_members(team_id)
            for i in members:
                if i["user_id"] == liaison_id:
                    raise ValidationFailed("the team liaison can not be a team member")
        elif "liaison_id" in updates:  # If liaison_id is null and definitely being changed
            # Ensure this team isn't registered for any events that require a liaison
            await can_team_remove_liaison(team_id)
        if "team_lead_id" in updates:
            team_lead_id = updates.get("team_lead_id")
            if not team_lead_id:
                raise ValidationFailed("team must have a captain")
            await can_update_team_lead(team_id, team_lead_id)
    return
