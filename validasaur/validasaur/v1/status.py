from typing import Any

from fastapi import APIRouter, Response, status

from validasaur.auth import auth_ready
from validasaur.db import db_ready

router = APIRouter()


@router.get("/livez")
def livez_check() -> dict:
    return {"status": "ok"}


@router.get("/readyz")
async def readyz_check(verbose: Any = None, response: Response = Response()) -> dict:
    ret = {"status": "ok"}
    ready = True

    okay, message = auth_ready()
    ret["auth"] = message
    ready = okay and ready

    okay, message = await db_ready()
    ret["db"] = message
    ready = okay and ready

    if not ready:
        ret["status"] = "not ready"
        response.status_code = status.HTTP_503_SERVICE_UNAVAILABLE

    if verbose is not None:
        return ret

    return {"status": ret["status"]}
