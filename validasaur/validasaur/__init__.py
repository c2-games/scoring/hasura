import uvicorn
from fastapi import FastAPI
from fastapi.exceptions import RequestValidationError
from starlette.middleware import Middleware
from starlette.middleware.cors import CORSMiddleware
from starlette.requests import Request
from starlette.responses import JSONResponse

import validasaur.v1
from validasaur.models.exceptions import ValidationFailed

from .config import config

config.validate()

app = FastAPI(
    version="0.1.0",
    middleware=[
        Middleware(
            CORSMiddleware,
            allow_origins=["*"],
            allow_credentials=True,
            allow_methods=["*"],
            allow_headers=["*"],
        ),
    ],
)
app.include_router(validasaur.v1.router, prefix="/v1")


@app.exception_handler(ValidationFailed)
async def unicorn_exception_handler_validation(_: Request, exc: ValidationFailed) -> JSONResponse:
    return JSONResponse(
        status_code=400,
        content={"message": exc.message},
    )


@app.exception_handler(RequestValidationError)
async def unicorn_exception_handler_req_validation(_: Request, exc: RequestValidationError) -> JSONResponse:
    print(str(exc))
    return JSONResponse(
        status_code=400,
        content={"message": str(exc)},
    )


def dev(port: int = 9091, host: str = "0.0.0.0", reload: bool = True) -> None:
    uvicorn.run("validasaur:app", host=host, port=port, reload=reload, loop="asyncio")


def prod(**kwargs) -> None:  # type: ignore [no-untyped-def]
    dev(**kwargs, reload=False)
