DROP TABLE "wrapup"."snapshots_service";
DROP TABLE "wrapup"."snapshots_bonus";
DROP TABLE "wrapup"."snapshots";
DROP TABLE "wrapup"."feedback";
DROP TABLE "wrapup"."awards";

drop schema "wrapup" cascade;
