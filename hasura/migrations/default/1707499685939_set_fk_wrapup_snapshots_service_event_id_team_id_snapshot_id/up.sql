alter table "wrapup"."snapshots_service"
  add constraint "snapshots_service_event_id_team_id_snapshot_id_fkey"
  foreign key ("event_id", "team_id", "snapshot_id")
  references "wrapup"."snapshots"
  ("event_id", "team_id", "id") on update restrict on delete restrict;
