
DROP VIEW "scoreboard"."team_summary";
DROP VIEW "scoreboard"."score";
DROP VIEW "scoring"."total_score";
DROP MATERIALIZED VIEW "scoring"."bonus_score";
CREATE OR REPLACE MATERIALIZED VIEW scoring.bonus_score AS
 SELECT bonus_points.event_id,
    bonus_points.team_id,
    sum(bonus_points.points) AS score
   FROM scoring.bonus_points
  GROUP BY bonus_points.event_id, bonus_points.team_id;
CREATE OR REPLACE VIEW scoring.total_score AS
 WITH summary AS (
         WITH entries AS (
                 SELECT bonus_score.event_id,
                    bonus_score.team_id,
                    bonus_score.score AS bonus_score,
                    0 AS ctf_score,
                    0 AS service_score
                   FROM scoring.bonus_score
                UNION ALL
                 SELECT ctf_score.event_id,
                    ctf_score.team_id,
                    0 AS bonus_score,
                    ctf_score.score AS ctf_score,
                    0 AS service_score
                   FROM scoring.ctf_score
                UNION ALL
                 SELECT service_score.event_id,
                    service_score.team_id,
                    0 AS bonus_score,
                    0 AS ctf_score,
                    service_score.score AS service_score
                   FROM scoring.service_score
                )
         SELECT entries.event_id,
            entries.team_id,
            sum(entries.bonus_score) AS bonus,
            sum(entries.ctf_score) AS ctf,
            sum(entries.service_score) AS service
           FROM entries
          GROUP BY entries.event_id, entries.team_id
        )
 SELECT summary.event_id,
    summary.team_id,
    summary.bonus,
    summary.ctf,
    summary.service,
    ((summary.bonus + summary.ctf) + summary.service) AS total
   FROM summary;
CREATE OR REPLACE VIEW scoreboard.score AS
 SELECT ts.event_id,
    ts.team_id,
    COALESCE(score.bonus, (0)::real) AS bonus,
    COALESCE(score.ctf, (0)::real) AS ctf,
    COALESCE((score.service)::double precision, (0)::double precision) AS service,
    COALESCE((score.total)::double precision, (0)::double precision) AS total,
    ts.visible
   FROM (scoreboard.team_settings ts
     LEFT JOIN ( SELECT total_score.event_id,
            total_score.team_id,
            total_score.bonus,
            total_score.ctf,
            total_score.service,
            total_score.total
           FROM scoring.total_score) score ON (((score.team_id = ts.team_id) AND (score.event_id = ts.event_id))));
CREATE OR REPLACE VIEW scoreboard.team_summary AS
 SELECT ts.event_id,
    ts.team_id,
    ts.visible,
    ts."order"
   FROM scoreboard.team_settings ts;

ALTER TABLE "scoring"."bonus_points" ALTER COLUMN "category" drop default;

alter table "scoring"."bonus_points" drop column "category";
