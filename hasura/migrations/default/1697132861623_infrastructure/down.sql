
-- Could not auto-generate a down migration.
-- Please write an appropriate down migration for the SQL below:
-- alter table "infrastructure"."jobs" add column "modified" timestamptz
--  not null default now();

-- Could not auto-generate a down migration.
-- Please write an appropriate down migration for the SQL below:
-- alter table "infrastructure"."jobs" add column "created" timestamptz
--  not null default now();


DROP INDEX IF EXISTS "infrastructure"."status_idx";

-- Could not auto-generate a down migration.
-- Please write an appropriate down migration for the SQL below:
-- alter table "infrastructure"."jobs" add column "plugin" text
--  null;

DROP INDEX IF EXISTS "infrastructure"."deployment_id_idx";

alter table "infrastructure"."jobs" drop constraint "jobs_deployment_id_fkey";

-- Could not auto-generate a down migration.
-- Please write an appropriate down migration for the SQL below:
-- alter table "infrastructure"."jobs" add column "deployment_id" uuid
--  not null;

alter table "infrastructure"."network_ips" drop constraint "network_ips_network_id_fkey";

alter table "infrastructure"."network_ips" drop constraint "network_ips_system_id_fkey",
  add constraint "network_ips_id_fkey"
  foreign key ("id")
  references "infrastructure"."systems"
  ("id") on update cascade on delete cascade;

alter table "infrastructure"."network_ips" drop constraint "network_ips_id_fkey";

DROP TABLE "infrastructure"."network_ips";

DROP TABLE "infrastructure"."systems";

DROP TABLE "infrastructure"."deployments";

DROP TABLE "infrastructure"."scenarios_templates";

DROP TABLE "infrastructure"."scenarios";

DROP TABLE "infrastructure"."templates";

DROP TABLE "infrastructure"."networks";

DROP TABLE "infrastructure"."jobs";

drop schema "infrastructure" cascade;
