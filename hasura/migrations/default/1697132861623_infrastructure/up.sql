
create schema "infrastructure";

CREATE TABLE "infrastructure"."jobs" ("id" uuid NOT NULL DEFAULT gen_random_uuid(), "requester" uuid NOT NULL, "status" text NOT NULL, "details" jsonb NOT NULL, PRIMARY KEY ("id") );
CREATE EXTENSION IF NOT EXISTS pgcrypto;

CREATE TABLE "infrastructure"."networks" ("id" uuid NOT NULL DEFAULT gen_random_uuid(), "name" text NOT NULL, "shared" boolean NOT NULL, "overlay" text, "address_space" text NOT NULL, "ipam" boolean NOT NULL, "wan" boolean NOT NULL, PRIMARY KEY ("id") , UNIQUE ("name"));
CREATE EXTENSION IF NOT EXISTS pgcrypto;

CREATE TABLE "infrastructure"."templates" ("id" uuid NOT NULL DEFAULT gen_random_uuid(), "type" text NOT NULL, "identifier" text NOT NULL, PRIMARY KEY ("id") );
CREATE EXTENSION IF NOT EXISTS pgcrypto;

CREATE TABLE "infrastructure"."scenarios" ("id" uuid NOT NULL DEFAULT gen_random_uuid(), "name" text NOT NULL, "description" text NOT NULL, "related_materials" jsonb NOT NULL, PRIMARY KEY ("id") , UNIQUE ("name"));
CREATE EXTENSION IF NOT EXISTS pgcrypto;

CREATE TABLE "infrastructure"."scenarios_templates" ("scenario_id" uuid NOT NULL, "template_id" uuid NOT NULL, PRIMARY KEY ("scenario_id","template_id") , FOREIGN KEY ("scenario_id") REFERENCES "infrastructure"."scenarios"("id") ON UPDATE cascade ON DELETE cascade, FOREIGN KEY ("template_id") REFERENCES "infrastructure"."templates"("id") ON UPDATE cascade ON DELETE cascade);

CREATE TABLE "infrastructure"."deployments" ("id" uuid NOT NULL DEFAULT gen_random_uuid(), "user_id" uuid NOT NULL, "state" text NOT NULL, "scenario_id" UUID NOT NULL, "created" timestamptz NOT NULL DEFAULT now(), "updated" timestamptz NOT NULL DEFAULT now(), PRIMARY KEY ("id") , FOREIGN KEY ("scenario_id") REFERENCES "infrastructure"."scenarios"("id") ON UPDATE cascade ON DELETE cascade, UNIQUE ("user_id", "scenario_id"));
CREATE EXTENSION IF NOT EXISTS pgcrypto;

CREATE TABLE "infrastructure"."systems" ("id" uuid NOT NULL DEFAULT gen_random_uuid(), "identifier" text NOT NULL, "type" text NOT NULL, "deployment_id" uuid NOT NULL, PRIMARY KEY ("id") , FOREIGN KEY ("deployment_id") REFERENCES "infrastructure"."deployments"("id") ON UPDATE cascade ON DELETE cascade, UNIQUE ("identifier"));
CREATE EXTENSION IF NOT EXISTS pgcrypto;

CREATE TABLE "infrastructure"."network_ips" ("id" uuid NOT NULL DEFAULT gen_random_uuid(), "network_id" uuid NOT NULL, "system_id" uuid NOT NULL, "address" text NOT NULL, PRIMARY KEY ("id") , UNIQUE ("network_id", "address"));
CREATE EXTENSION IF NOT EXISTS pgcrypto;

alter table "infrastructure"."network_ips"
  add constraint "network_ips_id_fkey"
  foreign key ("id")
  references "infrastructure"."systems"
  ("id") on update cascade on delete cascade;

alter table "infrastructure"."network_ips" drop constraint "network_ips_id_fkey",
  add constraint "network_ips_system_id_fkey"
  foreign key ("system_id")
  references "infrastructure"."systems"
  ("id") on update cascade on delete cascade;

alter table "infrastructure"."network_ips"
  add constraint "network_ips_network_id_fkey"
  foreign key ("network_id")
  references "infrastructure"."networks"
  ("id") on update cascade on delete cascade;


alter table "infrastructure"."jobs" add column "deployment_id" uuid
 not null;

alter table "infrastructure"."jobs"
  add constraint "jobs_deployment_id_fkey"
  foreign key ("deployment_id")
  references "infrastructure"."deployments"
  ("id") on update cascade on delete cascade;

CREATE  INDEX "deployment_id_idx" on
  "infrastructure"."jobs" using btree ("deployment_id");

alter table "infrastructure"."jobs" add column "plugin" text
 null;

CREATE  INDEX "status_idx" on
  "infrastructure"."jobs" using btree ("status");

alter table "infrastructure"."jobs" add column "created" timestamptz
 not null default now();

alter table "infrastructure"."jobs" add column "modified" timestamptz
 not null default now();
