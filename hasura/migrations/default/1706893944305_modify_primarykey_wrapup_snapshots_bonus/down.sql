alter table "wrapup"."snapshots_bonus" drop constraint "snapshots_bonus_pkey";
alter table "wrapup"."snapshots_bonus"
    add constraint "snapshots_bonus_pkey"
    primary key ("snapshot_id");
