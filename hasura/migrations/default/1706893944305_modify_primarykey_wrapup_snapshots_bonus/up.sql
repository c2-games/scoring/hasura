BEGIN TRANSACTION;
ALTER TABLE "wrapup"."snapshots_bonus" DROP CONSTRAINT "snapshots_bonus_pkey";

ALTER TABLE "wrapup"."snapshots_bonus"
    ADD CONSTRAINT "snapshots_bonus_pkey" PRIMARY KEY ("snapshot_id", "category");
COMMIT TRANSACTION;
