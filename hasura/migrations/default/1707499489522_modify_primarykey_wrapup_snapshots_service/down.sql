alter table "wrapup"."snapshots_service" drop constraint "snapshots_service_pkey";
alter table "wrapup"."snapshots_service"
    add constraint "snapshots_service_pkey"
    primary key ("snapshot_id", "check_id");
