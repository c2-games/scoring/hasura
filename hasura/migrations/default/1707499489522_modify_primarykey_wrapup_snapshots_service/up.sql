BEGIN TRANSACTION;
ALTER TABLE "wrapup"."snapshots_service" DROP CONSTRAINT "snapshots_service_pkey";

ALTER TABLE "wrapup"."snapshots_service"
    ADD CONSTRAINT "snapshots_service_pkey" PRIMARY KEY ("snapshot_id", "check_id", "team_id", "event_id");
COMMIT TRANSACTION;
