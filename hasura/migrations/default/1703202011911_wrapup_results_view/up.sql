CREATE OR REPLACE VIEW "wrapup"."results" AS
    WITH recent AS (
        SELECT
            max(time) as time,
            event_id,
            team_id,
            (SELECT max(total_score) FROM "wrapup"."snapshots" s WHERE s.event_id = ws.event_id) AS top_score
        FROM "wrapup"."snapshots" ws
        GROUP BY event_id, team_id
        ORDER BY time DESC
    )
    
    SELECT
        s.id AS snapshot_id,
        s.event_id,
        s.team_id AS top_score,
        s.total_score AS score
    FROM "wrapup"."snapshots" s
    INNER JOIN recent r
    ON r.time = s.time AND r.event_id = s.event_id AND r.team_id = s.team_id AND s.total_score = r.top_score;
