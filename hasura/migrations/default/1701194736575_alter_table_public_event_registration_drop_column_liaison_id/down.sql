comment on column "public"."event_registration"."liaison_id" is E'Teams registered for an event';
alter table "public"."event_registration" alter column "liaison_id" drop not null;
alter table "public"."event_registration" add column "liaison_id" uuid;
