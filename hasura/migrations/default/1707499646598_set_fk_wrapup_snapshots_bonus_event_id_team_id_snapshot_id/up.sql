alter table "wrapup"."snapshots_bonus"
  add constraint "snapshots_bonus_event_id_team_id_snapshot_id_fkey"
  foreign key ("event_id", "team_id", "snapshot_id")
  references "wrapup"."snapshots"
  ("event_id", "team_id", "id") on update cascade on delete cascade;
