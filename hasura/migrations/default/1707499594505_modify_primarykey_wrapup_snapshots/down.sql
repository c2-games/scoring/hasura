alter table "wrapup"."snapshots" drop constraint "snapshots_pkey";
alter table "wrapup"."snapshots"
    add constraint "snapshots_pkey"
    primary key ("id");
