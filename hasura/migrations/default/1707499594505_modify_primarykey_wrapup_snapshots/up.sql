BEGIN TRANSACTION;
ALTER TABLE "wrapup"."snapshots" DROP CONSTRAINT "snapshots_pkey";

ALTER TABLE "wrapup"."snapshots"
    ADD CONSTRAINT "snapshots_pkey" PRIMARY KEY ("id", "event_id", "team_id");
COMMIT TRANSACTION;
