alter table "scoreboard"."team_settings" drop constraint "team_settings_order_key";
alter table "scoreboard"."team_settings" add constraint "team_settings_order_event_id_key" unique ("order", "event_id");
