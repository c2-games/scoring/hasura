comment on column "public"."environment_settings"."tapi_url" is E'Settings for all the parts of the environment';
alter table "public"."environment_settings" alter column "tapi_url" drop not null;
alter table "public"."environment_settings" add column "tapi_url" text;
