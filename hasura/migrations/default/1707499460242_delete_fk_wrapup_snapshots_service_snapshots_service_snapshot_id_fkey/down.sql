alter table "wrapup"."snapshots_service"
  add constraint "snapshots_service_snapshot_id_fkey"
  foreign key ("snapshot_id")
  references "wrapup"."snapshots"
  ("id") on update cascade on delete cascade;
