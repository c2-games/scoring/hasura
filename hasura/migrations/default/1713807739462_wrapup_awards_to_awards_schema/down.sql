
CREATE TABLE "wrapup"."awards" ("id" uuid NOT NULL DEFAULT gen_random_uuid(), "type" text NOT NULL, "img" text, "img_dark" text, "event_id" uuid, "team_id" uuid, "user_id" uuid NOT NULL, "created" timestamptz NOT NULL DEFAULT now(), PRIMARY KEY ("id") , FOREIGN KEY ("team_id") REFERENCES "public"."teams"("id") ON UPDATE cascade ON DELETE restrict, FOREIGN KEY ("event_id") REFERENCES "public"."events"("id") ON UPDATE cascade ON DELETE cascade);

INSERT INTO wrapup.awards (id, type, img, img_dark, event_id, team_id, user_id, created) SELECT a.id, b.name as type, b.img, b.img_dark, event_id, team_id, user_id, created FROM awards.assertions a JOIN (SELECT badge.id, name, img, img_dark FROM awards.badges badge) b ON b.id = badge_id;

DROP TABLE "awards"."assertions";

DROP TABLE "awards"."badges";

drop schema "awards" cascade;
