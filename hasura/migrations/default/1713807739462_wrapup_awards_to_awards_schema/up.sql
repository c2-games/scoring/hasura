
create schema "awards";

CREATE TABLE "awards"."badges" ("id" uuid NOT NULL DEFAULT gen_random_uuid(), "name" text NOT NULL, "img" text, "img_dark" text, "badgr_badge_id" text, "created" timestamptz NOT NULL DEFAULT now(), "modified" timestamptz NOT NULL DEFAULT now(), PRIMARY KEY ("id") , UNIQUE ("badgr_badge_id"));
CREATE EXTENSION IF NOT EXISTS pgcrypto;

CREATE TABLE "awards"."assertions" ("id" uuid NOT NULL DEFAULT gen_random_uuid(), "badge_id" uuid NOT NULL, "event_id" uuid, "team_id" uuid, "user_id" uuid NOT NULL, "badgr_assertion_id" text, "created" timestamptz NOT NULL DEFAULT now(), "modified" timestamptz NOT NULL DEFAULT now(), PRIMARY KEY ("id") , FOREIGN KEY ("badge_id") REFERENCES "awards"."badges"("id") ON UPDATE cascade ON DELETE cascade, FOREIGN KEY ("event_id") REFERENCES "public"."events"("id") ON UPDATE cascade ON DELETE cascade, FOREIGN KEY ("team_id") REFERENCES "public"."teams"("id") ON UPDATE cascade ON DELETE cascade, UNIQUE ("badgr_assertion_id"));COMMENT ON TABLE "awards"."assertions" IS E'Assertions are representations of an awarded badge';
CREATE EXTENSION IF NOT EXISTS pgcrypto;

INSERT INTO awards.badges (name, img, img_dark) SELECT DISTINCT ON (type) type, img, img_dark from wrapup.awards;

INSERT INTO awards.assertions (id, badge_id, event_id, team_id, user_id, created, modified) SELECT id, (SELECT id FROM awards.badges b WHERE b.name = type) as badge_id, event_id, team_id, user_id, created, created as modified from wrapup.awards;

DROP table "wrapup"."awards";
