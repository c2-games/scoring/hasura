SET check_function_bodies = false;
CREATE SCHEMA challenges;
CREATE SCHEMA checks;
CREATE SCHEMA scoreboard;
CREATE SCHEMA scoring;
CREATE SCHEMA storage;
CREATE SCHEMA variables;
CREATE TABLE challenges.active (
    event_id uuid NOT NULL,
    challenge_id uuid NOT NULL,
    release_time timestamp with time zone,
    points real NOT NULL
);
COMMENT ON TABLE challenges.active IS 'challenges allocated to an event that are released or will be released';
CREATE FUNCTION challenges.active_released(challenge_row challenges.active) RETURNS boolean
    LANGUAGE sql STABLE
    AS $$
SELECT EXISTS (
    SELECT 1
    FROM challenges.challenges
    WHERE now() >= challenge_row.release_time AND challenge_row.challenge_id = id
);
$$;
CREATE FUNCTION checks.row_update_adversarial_status() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
BEGIN
    -- NEW.* -- inserted check result
    -- Exit early if non-adversarial type
    IF 'adversarial'::text <> (SELECT c."type" FROM checks.checks c WHERE c.id = NEW.check_id) THEN
        RETURN NULL;
    END IF;
    -- Look up adversarial status that matches this event id / check id / team id
    -- Insert fresh status if it doesn't exist, this will self-select
    -- on pkey for conflicts
    INSERT INTO checks.adversarial_status AS ss
    VALUES (
        NEW.event_id,
        NEW.check_id,
        NEW.team_id,
        NEW.result_code,
        NEW.source,
        NEW.check_time,
        NEW.participant,
        NEW.staff,
        NEW.data
    )
    ON CONFLICT ON CONSTRAINT adversarial_status_pkey
    DO
        -- Update status and related info
        UPDATE SET (
            result_code,
            source,
            check_time,
            participant,
            staff,
            data
        ) = (
            NEW.result_code,
            NEW.source,
            NEW.check_time,
            NEW.participant,
            NEW.staff,
            NEW.data
        )
        -- Ensure that only the latest check status is applied
        WHERE ss.check_time < NEW.check_time;
    -- If status changed between success and not success, track those time stamps
    IF 'success' = NEW.result_code THEN
        UPDATE checks.adversarial_status ss SET last_success = NEW.check_time
        WHERE ss.event_id = NEW.event_id AND ss.check_id = NEW.check_id AND ss.team_id = NEW.team_id;
    ELSIF 'partial' = NEW.result_code THEN
        UPDATE checks.adversarial_status ss SET last_partial = NEW.check_time
        WHERE ss.event_id = NEW.event_id AND ss.check_id = NEW.check_id AND ss.team_id = NEW.team_id;
    ELSIF 'failure' = NEW.result_code THEN
        UPDATE checks.adversarial_status ss SET last_failure = NEW.check_time
        WHERE ss.event_id = NEW.event_id AND ss.check_id = NEW.check_id AND ss.team_id = NEW.team_id;
    ELSIF 'error' = NEW.result_code THEN
        UPDATE checks.adversarial_status ss SET last_error = NEW.check_time
        WHERE ss.event_id = NEW.event_id AND ss.check_id = NEW.check_id AND ss.team_id = NEW.team_id;
    END IF;
    RETURN NULL;
END;
$$;
CREATE FUNCTION checks.row_update_service_status() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
BEGIN
    -- NEW.* -- inserted check result
    -- Exit early if non-service type
    IF 'service'::text <> (SELECT c."type" FROM checks.checks c WHERE c.id = NEW.check_id) THEN
        RETURN NULL;
    END IF;
    -- Look up service status that matches this event id / check id / team id
    -- Insert fresh status if it doesn't exist, this will self-select
    -- on pkey for conflicts
    INSERT INTO checks.service_status AS ss
    VALUES (
        NEW.event_id,
        NEW.check_id,
        NEW.team_id,
        NEW.result_code,
        NEW.target,
        NEW.source,
        NEW.check_time,
        NEW.participant,
        NEW.staff,
        NEW.data
    )
    ON CONFLICT ON CONSTRAINT service_status_pkey
    DO
        -- Update status and related info
        UPDATE SET (
            result_code,
            target,
            source,
            check_time,
            participant,
            staff,
            data
        ) = (
            NEW.result_code,
            NEW.target,
            NEW.source,
            NEW.check_time,
            NEW.participant,
            NEW.staff,
            NEW.data
        )
        -- Ensure that only the latest check status is applied
        WHERE ss.check_time < NEW.check_time;
    -- If status changed between success and not success, track those time stamps
    IF 'success' = NEW.result_code THEN
        UPDATE checks.service_status ss SET last_success = NEW.check_time
        WHERE ss.event_id = NEW.event_id AND ss.check_id = NEW.check_id AND ss.team_id = NEW.team_id;
    ELSIF 'partial' = NEW.result_code THEN
        UPDATE checks.service_status ss SET last_partial = NEW.check_time
        WHERE ss.event_id = NEW.event_id AND ss.check_id = NEW.check_id AND ss.team_id = NEW.team_id;
    ELSIF 'failure' = NEW.result_code THEN
        UPDATE checks.service_status ss SET last_failure = NEW.check_time
        WHERE ss.event_id = NEW.event_id AND ss.check_id = NEW.check_id AND ss.team_id = NEW.team_id;
    ELSIF 'error' = NEW.result_code THEN
        UPDATE checks.service_status ss SET last_error = NEW.check_time
        WHERE ss.event_id = NEW.event_id AND ss.check_id = NEW.check_id AND ss.team_id = NEW.team_id;
    END IF;
    RETURN NULL;
END;
$$;
CREATE TABLE public.events (
    id uuid DEFAULT gen_random_uuid() NOT NULL,
    name text NOT NULL,
    start timestamp with time zone,
    "end" timestamp with time zone,
    CONSTRAINT start_before_end CHECK ((start <= "end"))
);
CREATE FUNCTION public.event_active(events_row public.events) RETURNS boolean
    LANGUAGE plpgsql IMMUTABLE
    AS $$
BEGIN
    IF events_row.start IS NULL AND events_row."end" IS NULL THEN
        RETURN true;
    ELSIF events_row.start IS NULL AND events_row."end" IS NOT NULL THEN
        RETURN NOT public.event_ended(events_row);
    ELSIF events_row.start IS NOT NULL AND events_row."end" IS NULL THEN
        RETURN public.event_started(events_row);
    END IF;
    RETURN EXISTS (
        SELECT 1
        FROM public.events
        WHERE (now() BETWEEN events_row.start AND events_row."end") AND events_row.id = id
    );
END;
$$;
CREATE FUNCTION public.event_ended(events_row public.events) RETURNS boolean
    LANGUAGE plpgsql IMMUTABLE
    AS $$
BEGIN
    IF events_row."end" IS NULL THEN
        RETURN false;
    END IF;
    RETURN EXISTS (
        SELECT 1
        FROM public.events
        WHERE now() > events_row."end" AND events_row.id = id
    );
END;
$$;
CREATE FUNCTION public.event_started(events_row public.events) RETURNS boolean
    LANGUAGE plpgsql IMMUTABLE
    AS $$
BEGIN
    IF events_row.start IS NULL THEN
        RETURN true;
    END IF;
    RETURN EXISTS (
        SELECT 1
        FROM public.events
        WHERE now() >= events_row.start AND events_row.id = id
    );
END;
$$;
CREATE FUNCTION public.insert_event_registration_validate_event_group() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
BEGIN
    IF (
        WITH settings AS (
            SELECT
                NEW.event_id,
                eg.*,
                (SELECT COUNT(*) FROM event_registration er WHERE ege.event_id = er.event_id) AS registered_teams,
                (SELECT COUNT(*) FROM (event_registration er INNER JOIN teams t ON er.team_id = t.id) WHERE team.institution_id = t.institution_id) AS institution_teams,
                (SELECT COUNT(*) FROM (event_registration er INNER JOIN teams t ON er.team_id = t.id) WHERE team.institution_id = t.institution_id AND er.event_id = NEW.event_id) AS event_institution_teams,
                (
                    SELECT COUNT(*)
                    FROM (
                        event_registration er
                        INNER JOIN teams t ON er.team_id = t.id
                        INNER JOIN event_group_event iege ON er.event_id = iege.event_id
                        INNER JOIN event_groups ieg ON ieg.id = iege.event_group_id
                    ) WHERE ieg.id = ege.event_group_id AND team.institution_id = t.institution_id AND er.event_id <> NEW.event_id
                ) AS other_event_institution_teams
            FROM (event_group_event ege
                INNER JOIN event_groups eg ON eg.id = ege.event_group_id
                INNER JOIN teams team ON team.id = NEW.team_id
            )
            WHERE ege.event_id = NEW.event_id
        )
        -- Check if this is a valid registration when compared to the event group settings
        SELECT (
            -- If there is still room for teams on this event
            registered_teams < max_teams_per_event
            -- If there is still room for this institution to register teams for this event
            AND event_institution_teams < max_institution_teams_per_event
            -- If all teams from this institution, only have teams registered for this event (if this setting is toggled)
            AND ((NOT institution_teams_same_event) OR other_event_institution_teams < 1)
        )
        FROM settings
        WHERE event_id = NEW.event_id
        LIMIT 1
    ) THEN
        RETURN NEW;
    END IF;
    -- Do not perform the insert if checks fail
    RETURN NULL;
END;
$$;
CREATE FUNCTION public.insert_event_registration_validate_region() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
DECLARE
    team_regions uuid[];
    event_region uuid;
BEGIN
    -- Get the event region
    SELECT
        region_id INTO event_region
    FROM event_registration_settings
    WHERE event_id = NEW.event_id;
    -- If the event_region is not set, allow any team to sign up
    IF (event_region IS NULL) THEN RETURN NEW; END IF;
    -- Get a list of regions related to this team
    SELECT
        array(
            SELECT
                region_id
            FROM institution_region
            WHERE t.institution_id = institution_id
        ) INTO team_regions
    FROM teams t
    WHERE t.id = NEW.team_id;
    -- If the event_region is in the list of team regions, then allow signup
    IF (SELECT event_region = ANY (team_regions::uuid[])) THEN
        RETURN NEW;
    ELSE
        RETURN NULL;
    END IF;
END;
$$;
CREATE TABLE public.teams (
    id uuid DEFAULT gen_random_uuid() NOT NULL,
    name text NOT NULL,
    tapi_id integer,
    long_name text,
    team_lead_id uuid,
    institution_id uuid
);
CREATE FUNCTION public.max_team_size(teams_row public.teams) RETURNS integer
    LANGUAGE plpgsql IMMUTABLE
    AS $$
BEGIN
    RETURN (
        SELECT
            (SELECT max_team_size FROM event_registration_settings WHERE event_id = er.event_id) AS max_team_size
        FROM event_registration er
        WHERE team_id = teams_row.id
        ORDER BY max_team_size ASC
        LIMIT 1
    );
END;
$$;
CREATE FUNCTION public.refresh_ctf_views() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
BEGIN
    REFRESH MATERIALIZED VIEW "challenges"."solves";
    REFRESH MATERIALIZED VIEW "scoring"."ctf_score";
    RETURN NULL;
END;
$$;
CREATE TABLE public.event_registration_settings (
    event_id uuid NOT NULL,
    registration_open timestamp with time zone,
    registration_close timestamp with time zone,
    region_id uuid,
    max_team_size integer DEFAULT 10,
    min_team_size integer DEFAULT 2,
    roster_change_close timestamp with time zone,
    CONSTRAINT registration_open_before_close CHECK ((registration_open <= registration_close))
);
CREATE FUNCTION public.registration_active(event_registration_settings_row public.event_registration_settings) RETURNS boolean
    LANGUAGE plpgsql IMMUTABLE
    AS $$
BEGIN
    IF event_registration_settings_row.registration_open = NULL THEN
        RETURN false;
    ELSIF event_registration_settings_row.registration_close = NULL THEN
        RETURN false;
    END IF;
    RETURN now() BETWEEN event_registration_settings_row.registration_open AND event_registration_settings_row.registration_close;
END;
$$;
CREATE FUNCTION public.roster_change_allowed(teams_row public.teams) RETURNS boolean
    LANGUAGE plpgsql IMMUTABLE
    AS $$
BEGIN
    RETURN COALESCE((
        SELECT
            (SELECT roster_change_close FROM event_registration_settings WHERE event_id = er.event_id AND registration_open < now()) AS roster_change_close
        FROM (event_registration er
            JOIN (
                SELECT
                    id,
                    start,
                    "end"
                FROM events
            ) e ON e.id = er.event_id
        )
        WHERE team_id = teams_row.id AND now() < e.start
        ORDER BY roster_change_close ASC
        LIMIT 1
    ) > now(), false);
    -- If roster_change_close is null, then do not allow changes
END;
$$;
CREATE FUNCTION public.team_full(teams_row public.teams) RETURNS boolean
    LANGUAGE plpgsql IMMUTABLE
    AS $$
BEGIN
    RETURN COALESCE(public.max_team_size(teams_row) <= public.team_size(teams_row), false);
END;
$$;
CREATE FUNCTION public.team_size(teams_row public.teams) RETURNS integer
    LANGUAGE plpgsql IMMUTABLE
    AS $$
BEGIN
    RETURN (
        SELECT COUNT(*)
        FROM team_membership
        WHERE team_id = teams_row.id
    );
END;
$$;
CREATE FUNCTION scoring.adjust_period_points() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
BEGIN
    IF (TG_OP = 'DELETE') THEN
        -- remove rows from points for this period id
        DELETE FROM scoring.points
        WHERE period_id = OLD.id;
    ELSIF (TG_OP = 'UPDATE') THEN
        -- perform full re-initialization on entries from points this period_id
        -- If the id changed, update all the points entries using the old id
        -- TODO: if ONLY the id changes and there is no start or end adjustment, then just that change is cascaded with no recomputation.
        IF (NEW.id <> OLD.id) THEN
            UPDATE
                scoring.points pts
            SET
                pts.period_id = NEW.id
            WHERE pts.period_id = OLD.id;
        END IF;
        -- A full initialization must be done as the new start or end time may
        -- encompass additional check results
        PERFORM
            scoring.init_period_score(pts.period_id, pts.check_id)
        FROM scoring.points pts
        WHERE pts.period_id = NEW.id;
    -- Don't do anything for inserts since there shouldn't be any points rows yet
    END IF;
    RETURN NULL;
END;
$$;
CREATE FUNCTION scoring.calculate_period_scores() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
BEGIN
    IF (TG_OP = 'DELETE') THEN
        -- remove rows from service_period_score for this period id and check id combo
        DELETE FROM scoring.service_period_score
        WHERE (period_id = OLD.period_id AND check_id = OLD.check_id);
    ELSIF (TG_OP = 'UPDATE') THEN
        -- re-initialize the rows for this period id and check id with data from check.results
        IF (NEW.period_id <> OLD.period_id OR NEW.check_id <> OLD.check_id) THEN
            UPDATE
                scoring.service_period_score sps
            SET
                sps.period_id = NEW.period_id,
                sps.check_id = NEW.check_id,
                max_points = NEW.points,
                score = sps.successful_team_checks * 1.0 / scoring.max_checks(NEW.period_id, NEW.check_id) * NEW.points
                --         multiply to cast to float ^^^
            WHERE sps.period_id = OLD.period_id AND sps.check_id = OLD.check_id;
        END IF;
        IF (NEW.interval <> OLD.interval) THEN
            -- If the interval changed, then a full initialization is needed
            -- since there might be old results to process, or there might be
            -- the wrong intervals in the check_intervals
            PERFORM scoring.init_period_score(NEW.period_id, NEW.check_id);
        ELSE
            -- If just the total points changed, then it is simpler
            PERFORM scoring.reinit_period_score(NEW.period_id, NEW.check_id);
        END IF;
    ELSIF (TG_OP = 'INSERT') THEN
        -- initialize rows for this period id and check id from check.results
        PERFORM scoring.init_period_score(NEW.period_id, NEW.check_id);
    END IF;
    RETURN NULL;
END;
$$;
CREATE FUNCTION scoring.init_period_score(pid uuid, cid uuid) RETURNS void
    LANGUAGE plpgsql
    AS $$
BEGIN
    -- Remove entry if exists from period scores
    DELETE FROM scoring.service_period_score
    WHERE (period_id = pid AND check_id = cid);
    -- Make a new entry if needed
    WITH pre_entries AS (
        SELECT
            DISTINCT ON (r.event_id, r.check_id, r.team_id, p.id, check_interval)
            r.event_id,
            r.check_id,
            r.team_id,
            p.id AS period_id,
            r.check_time,
            date_bin(pts.interval * interval '1 second', r.check_time, p.start) as check_interval,
            r.result_code,
            pts.points AS max_points
        FROM checks.results r
        INNER JOIN scoring.periods p
            ON p.id = pid AND p.event_id = r.event_id
        INNER JOIN scoring.points pts
            ON p.id = pts.period_id AND r.check_id = pts.check_id
        WHERE cid = r.check_id AND r.check_time BETWEEN p.start AND p."end"
        GROUP BY r.event_id, r.check_id, r.team_id, p.id, pts.interval, check_interval, result_code, check_time, max_points
        ORDER BY r.event_id, r.check_id, r.team_id, p.id, check_interval, check_time, team_id
    ),
    entries AS (
        SELECT
            e.event_id,
            e.check_id,
            e.team_id,
            e.period_id,
            COUNT(*) FILTER(WHERE e.result_code = 'success') AS successful_team_checks,
            COUNT(*) AS total_team_checks,
            scoring.max_checks(e.period_id, e.check_id) AS max_checks,
            e.max_points,
            MAX(e.check_time) as check_time,
            ARRAY_AGG(check_interval) AS check_intervals
        FROM pre_entries e
        GROUP BY event_id, check_id, team_id, period_id, max_points
    )
    -- make the inserts
    INSERT INTO scoring.service_period_score (
        event_id,
        check_id,
        team_id,
        period_id,
        successful_team_checks,
        total_team_checks,
        max_points,
        max_checks,
        check_time,
        check_intervals,
        score
    )
    SELECT
        et.event_id,
        et.check_id,
        et.team_id,
        et.period_id,
        et.successful_team_checks,
        et.total_team_checks,
        et.max_points,
        et.max_checks,
        et.check_time,
        et.check_intervals,
        et.successful_team_checks * 1.0 / et.max_checks * et.max_points AS score
    FROM entries et
    ON CONFLICT ON CONSTRAINT service_period_score_pkey
    DO NOTHING;
END;
$$;
CREATE FUNCTION scoring.is_in_period(t timestamp with time zone, pid uuid, cid uuid) RETURNS boolean
    LANGUAGE plpgsql
    AS $$
BEGIN
    RETURN EXISTS (
        SELECT 1
        FROM scoring.periods p
        WHERE p.id = pid AND t BETWEEN p.start AND p."end"
    );
END;
$$;
CREATE FUNCTION scoring.max_checks(pid uuid, cid uuid) RETURNS integer
    LANGUAGE plpgsql
    AS $$
DECLARE
    total integer;
BEGIN
    SELECT
        (EXTRACT(EPOCH FROM (p."end" - p.start))) / sp.interval INTO total
    FROM scoring.periods p
    INNER JOIN scoring.points sp ON p.id = sp.period_id
    WHERE pid = sp.period_id AND cid = sp.check_id;
    RETURN total;
END;
$$;
CREATE FUNCTION scoring.period_interval(pid uuid, cid uuid, t timestamp with time zone) RETURNS timestamp with time zone
    LANGUAGE plpgsql
    AS $$
BEGIN
    -- Use date_bin to get the interval this timestamp belongs to
    -- Note: This function will not check if the timestamp is beyond the period end
    RETURN (
        SELECT date_bin(pts.interval * interval '1 second', t, p.start)
        FROM scoring.points pts
        INNER JOIN scoring.periods p ON p.id = pts.period_id
        WHERE pts.period_id = pid AND pts.check_id = cid
    );
END;
$$;
CREATE FUNCTION scoring.refresh_bonus_score_view() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
BEGIN
    REFRESH MATERIALIZED VIEW "scoring"."bonus_score";
    RETURN NULL;
END;
$$;
CREATE FUNCTION scoring.reinit_period_score(pid uuid, cid uuid) RETURNS void
    LANGUAGE plpgsql
    AS $$
BEGIN
    -- If a service_period_score does not exist for this pid and cid,
    -- then initialize one and short circuit the operation and return.
    IF NOT EXISTS(
        SELECT *
        FROM scoring.service_period_score sps
        WHERE sps.period_id = pid AND sps.check_id = cid)
    THEN
        PERFORM scoring.init_period_score(pid, cid);
        RETURN;
    END IF;
    -- Update the max_points and score if the points or interval changed
    UPDATE
        scoring.service_period_score sps
    SET
        max_checks = scoring.max_checks(pid, cid),
        max_points = pts.points,
        score = sps.successful_team_checks * 1.0 / scoring.max_checks(pid, cid) * pts.points
        --         multiply to cast to float ^^^
    FROM scoring.points pts
    WHERE sps.period_id = pid AND pts.period_id = pid AND sps.check_id = cid AND pts.check_id = cid;
END;
$$;
CREATE FUNCTION scoring.row_update_period_scores() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
BEGIN
    -- NEW.* -- inserted check result
    -- Exit early if non-service type
    IF 'service'::text <> (SELECT c."type" FROM checks.checks c WHERE c.id = NEW.check_id) THEN
        RETURN NULL;
    END IF;
    -- Check if entry exists for this period/check/team
    IF NOT EXISTS(
        SELECT *
        FROM scoring.service_period_score sps
        WHERE
            sps.event_id = NEW.event_id AND
            sps.check_id = NEW.check_id AND
            sps.team_id = NEW.team_id AND
            scoring.is_in_period(NEW.check_time, sps.period_id, sps.check_id)
    ) THEN
        -- Make a stub entry and update it in the next step
        INSERT INTO scoring.service_period_score (
            event_id,
            check_id,
            team_id,
            period_id,
            max_points,
            max_checks,
            check_time
        )
        SELECT
            NEW.event_id,
            NEW.check_id,
            NEW.team_id,
            pts.period_id,
            pts.points AS max_points,
            scoring.max_checks(pts.period_id, pts.check_id) AS max_checks,
            sp.start AS check_time
        FROM scoring.points pts
        INNER JOIN scoring.periods sp ON sp.id = pts.period_id
        WHERE sp.event_id = NEW.event_id AND pts.check_id = NEW.check_id
        ON CONFLICT ON CONSTRAINT service_period_score_pkey
        DO NOTHING;
    END IF;
    -- TODO: this is kind of complicated still
    UPDATE
        scoring.service_period_score sps
    SET (
        successful_team_checks,
        total_team_checks,
        check_time,
        check_intervals,
        score
    ) = (
        -- If success, increment success checks
        CASE NEW.result_code
        WHEN 'success' THEN
            COALESCE(sps.successful_team_checks, 0)+1
        ELSE
            COALESCE(sps.successful_team_checks, 0)
        END,
        -- Increment total team checks
        sps.total_team_checks+1,
        -- Only update the check time here if it is newer
        GREATEST(NEW.check_time, sps.check_time),
        -- Add the check interval for this check
        array_append(
            sps.check_intervals,
            scoring.period_interval(sps.period_id, sps.check_id, NEW.check_time)
        ),
        -- If success check calculate updated score
        CASE NEW.result_code
        WHEN 'success' THEN
            ((COALESCE(sps.successful_team_checks, 0)+1)::real / sps.max_checks) * sps.max_points
        ELSE
            sps.score
        END
    )
    -- Look up period scores that match this event id / check id / team id
    --  and check time is between start/end of period
    WHERE (
        sps.event_id = NEW.event_id AND
        sps.check_id = NEW.check_id AND
        sps.team_id = NEW.team_id AND
        -- Check if this check belongs to this period
        scoring.is_in_period(NEW.check_time, sps.period_id, sps.check_id) AND
        -- Check if this check already has a point counted for the period interval
        NOT (scoring.period_interval(sps.period_id, sps.check_id, NEW.check_time) = ANY(sps.check_intervals))
    );
    RETURN NULL;
END;
$$;
CREATE FUNCTION storage.protect_default_bucket_delete() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
BEGIN
  IF OLD.ID = 'default' THEN
    RAISE EXCEPTION 'Can not delete default bucket';
  END IF;
  RETURN OLD;
END;
$$;
CREATE FUNCTION storage.protect_default_bucket_update() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
BEGIN
  IF OLD.ID = 'default' AND NEW.ID <> 'default' THEN
    RAISE EXCEPTION 'Can not rename default bucket';
  END IF;
  RETURN NEW;
END;
$$;
CREATE FUNCTION storage.set_current_timestamp_updated_at() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
DECLARE
  _new record;
BEGIN
  _new := new;
  _new. "updated_at" = now();
  RETURN _new;
END;
$$;
CREATE TABLE challenges.challenges (
    id uuid DEFAULT gen_random_uuid() NOT NULL,
    name text NOT NULL,
    description text NOT NULL,
    category text NOT NULL,
    designer text NOT NULL
);
CREATE TABLE challenges.files (
    event_id uuid NOT NULL,
    challenge_id uuid NOT NULL,
    file_id uuid NOT NULL,
    dangerous boolean DEFAULT false NOT NULL
);
COMMENT ON TABLE challenges.files IS 'CTF challenge files';
CREATE TABLE challenges.flags (
    event_id uuid NOT NULL,
    challenge_id uuid NOT NULL,
    flag text NOT NULL
);
CREATE TABLE challenges.submissions (
    challenge_id uuid NOT NULL,
    team_id uuid NOT NULL,
    flag text NOT NULL,
    submitter text NOT NULL,
    "time" timestamp with time zone DEFAULT now() NOT NULL,
    event_id uuid NOT NULL
);
CREATE MATERIALIZED VIEW challenges.solves AS
 WITH matches AS (
         SELECT DISTINCT ON (submissions.event_id, submissions.team_id, submissions.challenge_id) submissions.challenge_id,
            submissions.team_id,
            submissions.flag,
            submissions.submitter,
            submissions."time",
            submissions.event_id,
            m.cid,
            m.flag
           FROM (challenges.submissions submissions
             JOIN ( SELECT f.challenge_id AS cid,
                    f.flag,
                    f.event_id
                   FROM challenges.flags f) m ON (((m.cid = submissions.challenge_id) AND (m.event_id = submissions.event_id))))
          WHERE (submissions.flag = m.flag)
        )
 SELECT matches.event_id,
    matches.team_id,
    matches.challenge_id,
    matches.submitter,
    matches."time"
   FROM matches matches(challenge_id, team_id, flag, submitter, "time", event_id, cid, flag_1)
  ORDER BY matches.event_id, matches.team_id, matches.challenge_id
  WITH NO DATA;
CREATE TABLE challenges.tags (
    challenge_id uuid NOT NULL,
    tag text NOT NULL,
    scope text NOT NULL,
    color text
);
CREATE TABLE checks.adversarial_status (
    event_id uuid NOT NULL,
    check_id uuid NOT NULL,
    team_id uuid NOT NULL,
    result_code text NOT NULL,
    source text NOT NULL,
    check_time timestamp with time zone NOT NULL,
    participant jsonb,
    staff jsonb,
    data jsonb,
    last_success timestamp with time zone,
    last_partial timestamp with time zone,
    last_failure timestamp with time zone,
    last_error timestamp with time zone
);
CREATE TABLE checks.checks (
    id uuid DEFAULT gen_random_uuid() NOT NULL,
    type text NOT NULL,
    name text NOT NULL,
    display_name text NOT NULL,
    script text NOT NULL,
    args text,
    created timestamp with time zone NOT NULL,
    modified timestamp with time zone NOT NULL,
    description text
);
CREATE TABLE checks.deployed (
    id uuid DEFAULT gen_random_uuid() NOT NULL,
    check_id uuid NOT NULL,
    event_id uuid NOT NULL,
    team_id uuid NOT NULL,
    host text NOT NULL
);
CREATE TABLE checks.results (
    id uuid DEFAULT gen_random_uuid() NOT NULL,
    event_id uuid NOT NULL,
    check_id uuid NOT NULL,
    team_id uuid NOT NULL,
    target text NOT NULL,
    source text NOT NULL,
    check_time timestamp with time zone NOT NULL,
    result_code text NOT NULL,
    created timestamp with time zone DEFAULT now() NOT NULL,
    data jsonb,
    participant jsonb,
    staff jsonb
);
CREATE VIEW checks.milestone_completed AS
 WITH status AS (
         SELECT results.id,
            results.event_id,
            results.check_id,
            results.team_id,
            results.target,
            results.source,
            results.check_time,
            results.result_code,
            results.created,
            results.data,
            results.participant,
            results.staff,
            t.id,
            t.type,
            t.name,
            t.display_name,
            t.script,
            t.args,
            t.created,
            t.modified,
            t.description
           FROM (checks.results results
             JOIN ( SELECT c.id,
                    c.type,
                    c.name,
                    c.display_name,
                    c.script,
                    c.args,
                    c.created,
                    c.modified,
                    c.description
                   FROM checks.checks c) t ON ((t.id = results.check_id)))
          WHERE ((results.result_code = 'success'::text) AND (t.type = 'milestone'::text))
        )
 SELECT DISTINCT status.event_id,
    status.team_id,
    status.check_id
   FROM status status(id, event_id, check_id, team_id, target, source, check_time, result_code, created, data, participant, staff, id_1, type, name, display_name, script, args, created_1, modified, description)
  ORDER BY status.event_id, status.team_id, status.check_id;
CREATE TABLE checks.service_status (
    event_id uuid NOT NULL,
    check_id uuid NOT NULL,
    team_id uuid NOT NULL,
    result_code text NOT NULL,
    target text NOT NULL,
    source text NOT NULL,
    check_time timestamp with time zone NOT NULL,
    participant jsonb,
    staff jsonb,
    data jsonb,
    last_success timestamp with time zone,
    last_partial timestamp with time zone,
    last_failure timestamp with time zone,
    last_error timestamp with time zone
);
CREATE TABLE public.coaches (
    team_id uuid NOT NULL,
    user_id uuid NOT NULL
);
CREATE TABLE public.environment_settings (
    environment_id uuid NOT NULL,
    keycloak_url text NOT NULL,
    keycloak_realm text NOT NULL,
    storage_api text NOT NULL,
    storage_s3_api text,
    storage_bucket text NOT NULL,
    tapi_url text,
    extra jsonb
);
COMMENT ON TABLE public.environment_settings IS 'Settings for all the parts of the environment';
CREATE TABLE public.environments (
    id uuid DEFAULT gen_random_uuid() NOT NULL,
    event_id uuid NOT NULL,
    name text NOT NULL
);
CREATE TABLE public.event_group_event (
    event_group_id uuid NOT NULL,
    event_id uuid NOT NULL
);
CREATE TABLE public.event_groups (
    id uuid DEFAULT gen_random_uuid() NOT NULL,
    name text NOT NULL,
    max_institution_teams_per_event integer DEFAULT 3,
    max_teams_per_event integer DEFAULT 12,
    institution_teams_same_event boolean DEFAULT true NOT NULL
);
CREATE TABLE public.event_registration (
    event_id uuid NOT NULL,
    team_id uuid NOT NULL,
    liaison_id uuid
);
COMMENT ON TABLE public.event_registration IS 'Teams registered for an event';
CREATE TABLE public.institution_domains (
    institution_id uuid NOT NULL,
    domain text NOT NULL
);
CREATE TABLE public.institution_region (
    institution_id uuid NOT NULL,
    region_id uuid NOT NULL
);
CREATE TABLE public.institutions (
    id uuid DEFAULT gen_random_uuid() NOT NULL,
    name text NOT NULL
);
CREATE TABLE public.regions (
    id uuid DEFAULT gen_random_uuid() NOT NULL,
    name text NOT NULL
);
CREATE TABLE public.team_membership (
    team_id uuid NOT NULL,
    user_id uuid NOT NULL
);
CREATE TABLE scoreboard.displayed_services (
    event_id uuid NOT NULL,
    check_id uuid NOT NULL,
    "order" integer NOT NULL
);
CREATE TABLE scoreboard.team_settings (
    event_id uuid NOT NULL,
    team_id uuid NOT NULL,
    visible boolean NOT NULL,
    "order" integer NOT NULL
);
CREATE VIEW scoreboard.adversarial_status AS
 WITH teams_services_union AS (
         SELECT ds.event_id,
            ds.check_id,
            ts.team_id,
            ts.visible,
            ts."order" AS team_order,
            ds."order" AS check_order
           FROM ((scoreboard.displayed_services ds
             JOIN ( SELECT team_settings.event_id,
                    team_settings.team_id,
                    team_settings.visible,
                    team_settings."order"
                   FROM scoreboard.team_settings) ts ON ((ts.event_id = ds.event_id)))
             JOIN ( SELECT checks.id,
                    checks.type
                   FROM checks.checks
                  WHERE (checks.type = 'adversarial'::text)) ct ON ((ct.id = ds.check_id)))
        )
 SELECT tsu.event_id,
    tsu.check_id,
    tsu.team_id,
    tsu.visible,
    tsu.team_order,
    tsu.check_order,
    COALESCE(ss.result_code, 'pending'::text) AS result_code,
    ss.source,
    ss.check_time,
    ss.participant,
    ss.staff,
    ss.data,
    ss.last_success,
    ss.last_partial,
    ss.last_failure,
    ss.last_error
   FROM (teams_services_union tsu
     LEFT JOIN ( SELECT adversarial_status.event_id,
            adversarial_status.team_id,
            adversarial_status.check_id,
            adversarial_status.result_code,
            adversarial_status.source,
            adversarial_status.check_time,
            adversarial_status.participant,
            adversarial_status.staff,
            adversarial_status.data,
            adversarial_status.last_success,
            adversarial_status.last_partial,
            adversarial_status.last_failure,
            adversarial_status.last_error
           FROM checks.adversarial_status) ss ON (((ss.event_id = tsu.event_id) AND (ss.team_id = tsu.team_id) AND (ss.check_id = tsu.check_id))));
CREATE VIEW scoreboard.challenge_solves_by_category AS
SELECT
    NULL::uuid AS event_id,
    NULL::uuid AS team_id,
    NULL::boolean AS visible,
    NULL::text AS category,
    NULL::bigint AS solved,
    NULL::bigint AS total;
CREATE TABLE scoreboard.event_settings (
    event_id uuid NOT NULL,
    org_name text,
    org_link text,
    info_link text
);
CREATE TABLE scoreboard.kanban (
    id uuid DEFAULT gen_random_uuid() NOT NULL,
    event_id uuid NOT NULL,
    team_id uuid NOT NULL,
    ctf_challenge_id uuid,
    check_id uuid,
    assignee text,
    created_by text,
    status text NOT NULL,
    title text NOT NULL,
    description text NOT NULL,
    created_at timestamp with time zone DEFAULT now() NOT NULL
);
COMMENT ON COLUMN scoreboard.kanban.assignee IS 'Email Address of Assigned User';
COMMENT ON COLUMN scoreboard.kanban.created_by IS 'Email Address of Creating User';
CREATE TABLE scoring.bonus_points (
    id uuid DEFAULT gen_random_uuid() NOT NULL,
    event_id uuid NOT NULL,
    team_id uuid NOT NULL,
    points real NOT NULL,
    reason text,
    "time" timestamp with time zone DEFAULT now() NOT NULL
);
COMMENT ON TABLE scoring.bonus_points IS 'Table to track bonus points awarded to teams';
CREATE MATERIALIZED VIEW scoring.bonus_score AS
 SELECT bonus_points.event_id,
    bonus_points.team_id,
    sum(bonus_points.points) AS score
   FROM scoring.bonus_points
  GROUP BY bonus_points.event_id, bonus_points.team_id
  WITH NO DATA;
CREATE MATERIALIZED VIEW scoring.ctf_score AS
 SELECT s.event_id,
    s.team_id,
    sum(r.points) AS score,
    count(r.*) AS solves
   FROM (challenges.solves s
     JOIN ( SELECT a.event_id,
            a.challenge_id,
            a.points
           FROM challenges.active a) r ON (((s.event_id = r.event_id) AND (s.challenge_id = r.challenge_id))))
  GROUP BY s.event_id, s.team_id
  WITH NO DATA;
CREATE TABLE scoring.service_period_score (
    event_id uuid NOT NULL,
    check_id uuid NOT NULL,
    team_id uuid NOT NULL,
    period_id uuid NOT NULL,
    successful_team_checks integer DEFAULT 0 NOT NULL,
    total_team_checks integer DEFAULT 0 NOT NULL,
    max_checks integer DEFAULT 0 NOT NULL,
    score real DEFAULT 0 NOT NULL,
    max_points real DEFAULT 0 NOT NULL,
    check_time timestamp with time zone NOT NULL,
    check_intervals timestamp with time zone[] DEFAULT '{}'::timestamp with time zone[]
);
CREATE VIEW scoring.service_score AS
 SELECT service_period_score.event_id,
    service_period_score.team_id,
    sum(service_period_score.score) AS score
   FROM scoring.service_period_score
  GROUP BY service_period_score.event_id, service_period_score.team_id;
CREATE VIEW scoring.total_score AS
 WITH summary AS (
         WITH entries AS (
                 SELECT bonus_score.event_id,
                    bonus_score.team_id,
                    bonus_score.score AS bonus_score,
                    0 AS ctf_score,
                    0 AS service_score
                   FROM scoring.bonus_score
                UNION ALL
                 SELECT ctf_score.event_id,
                    ctf_score.team_id,
                    0 AS bonus_score,
                    ctf_score.score AS ctf_score,
                    0 AS service_score
                   FROM scoring.ctf_score
                UNION ALL
                 SELECT service_score.event_id,
                    service_score.team_id,
                    0 AS bonus_score,
                    0 AS ctf_score,
                    service_score.score AS service_score
                   FROM scoring.service_score
                )
         SELECT entries.event_id,
            entries.team_id,
            sum(entries.bonus_score) AS bonus,
            sum(entries.ctf_score) AS ctf,
            sum(entries.service_score) AS service
           FROM entries
          GROUP BY entries.event_id, entries.team_id
        )
 SELECT summary.event_id,
    summary.team_id,
    summary.bonus,
    summary.ctf,
    summary.service,
    ((summary.bonus + summary.ctf) + summary.service) AS total
   FROM summary;
CREATE VIEW scoreboard.score AS
 SELECT ts.event_id,
    ts.team_id,
    COALESCE(score.bonus, (0)::real) AS bonus,
    COALESCE(score.ctf, (0)::real) AS ctf,
    COALESCE((score.service)::double precision, (0)::double precision) AS service,
    COALESCE((score.total)::double precision, (0)::double precision) AS total,
    ts.visible
   FROM (scoreboard.team_settings ts
     LEFT JOIN ( SELECT total_score.event_id,
            total_score.team_id,
            total_score.bonus,
            total_score.ctf,
            total_score.service,
            total_score.total
           FROM scoring.total_score) score ON (((score.team_id = ts.team_id) AND (score.event_id = ts.event_id))));
CREATE VIEW scoreboard.service_status AS
 WITH teams_services_union AS (
         SELECT ds.event_id,
            ds.check_id,
            ts.team_id,
            ts.visible,
            ts."order" AS team_order,
            ds."order" AS service_order
           FROM ((scoreboard.displayed_services ds
             JOIN ( SELECT team_settings.event_id,
                    team_settings.team_id,
                    team_settings.visible,
                    team_settings."order"
                   FROM scoreboard.team_settings) ts ON ((ts.event_id = ds.event_id)))
             JOIN ( SELECT checks.id,
                    checks.type
                   FROM checks.checks
                  WHERE (checks.type = 'service'::text)) ct ON ((ct.id = ds.check_id)))
        )
 SELECT tsu.event_id,
    tsu.check_id,
    tsu.team_id,
    tsu.visible,
    tsu.team_order,
    tsu.service_order,
    COALESCE(ss.result_code, 'pending'::text) AS result_code,
    ss.target,
    ss.source,
    ss.check_time,
    ss.participant,
    ss.staff,
    ss.data,
    ss.last_success,
    ss.last_partial,
    ss.last_failure,
    ss.last_error
   FROM (teams_services_union tsu
     LEFT JOIN ( SELECT service_status.event_id,
            service_status.team_id,
            service_status.check_id,
            service_status.result_code,
            service_status.target,
            service_status.source,
            service_status.check_time,
            service_status.participant,
            service_status.staff,
            service_status.data,
            service_status.last_success,
            service_status.last_partial,
            service_status.last_failure,
            service_status.last_error
           FROM checks.service_status) ss ON (((ss.event_id = tsu.event_id) AND (ss.team_id = tsu.team_id) AND (ss.check_id = tsu.check_id))));
CREATE VIEW scoreboard.team_summary AS
 SELECT ts.event_id,
    ts.team_id,
    ts.visible,
    ts."order"
   FROM scoreboard.team_settings ts;
CREATE TABLE scoring.periods (
    id uuid DEFAULT gen_random_uuid() NOT NULL,
    event_id uuid NOT NULL,
    start timestamp with time zone NOT NULL,
    "end" timestamp with time zone NOT NULL,
    CONSTRAINT start_before_end CHECK ((start <= "end"))
);
CREATE VIEW scoring.breaks AS
 SELECT t1.event_id,
    t2."end" AS start,
    t1.start AS "end"
   FROM (( SELECT DISTINCT t1_1.event_id,
            t1_1.start,
            row_number() OVER (ORDER BY t1_1.event_id, t1_1.start) AS rn
           FROM scoring.periods t1_1
          WHERE (NOT (EXISTS ( SELECT t2_1.id,
                    t2_1.event_id,
                    t2_1.start,
                    t2_1."end"
                   FROM scoring.periods t2_1
                  WHERE ((t1_1.event_id = t2_1.event_id) AND (t1_1.start > t2_1.start) AND (t1_1.start < t2_1."end")))))) t1
     JOIN ( SELECT DISTINCT t1_1.event_id,
            t1_1."end",
            row_number() OVER (ORDER BY t1_1.event_id, t1_1."end") AS rn
           FROM scoring.periods t1_1
          WHERE (NOT (EXISTS ( SELECT t2_1.id,
                    t2_1.event_id,
                    t2_1.start,
                    t2_1."end"
                   FROM scoring.periods t2_1
                  WHERE ((t1_1.event_id = t2_1.event_id) AND (t1_1."end" > t2_1.start) AND (t1_1."end" < t2_1."end")))))) t2 ON (((t1.rn - 1) = t2.rn)))
  WHERE ((t1.event_id = t2.event_id) AND (t2."end" < t1.start));
CREATE TABLE scoring.points (
    period_id uuid NOT NULL,
    check_id uuid NOT NULL,
    points real NOT NULL,
    "interval" integer NOT NULL
);
CREATE TABLE storage.buckets (
    id text NOT NULL,
    created_at timestamp with time zone DEFAULT now() NOT NULL,
    updated_at timestamp with time zone DEFAULT now() NOT NULL,
    download_expiration integer DEFAULT 30 NOT NULL,
    min_upload_file_size integer DEFAULT 1 NOT NULL,
    max_upload_file_size integer DEFAULT 50000000 NOT NULL,
    cache_control text DEFAULT 'max-age=3600'::text,
    presigned_urls_enabled boolean DEFAULT true NOT NULL,
    CONSTRAINT download_expiration_valid_range CHECK (((download_expiration >= 1) AND (download_expiration <= 604800)))
);
CREATE TABLE storage.files (
    id uuid DEFAULT gen_random_uuid() NOT NULL,
    created_at timestamp with time zone DEFAULT now() NOT NULL,
    updated_at timestamp with time zone DEFAULT now() NOT NULL,
    bucket_id text DEFAULT 'default'::text NOT NULL,
    name text,
    size integer,
    mime_type text,
    etag text,
    is_uploaded boolean DEFAULT false,
    uploaded_by_user_id uuid
);
CREATE TABLE storage.schema_migrations (
    version bigint NOT NULL,
    dirty boolean NOT NULL
);
CREATE TABLE variables."check" (
    check_id uuid NOT NULL,
    vars jsonb NOT NULL
);
CREATE TABLE variables.deployment (
    deploy_id uuid NOT NULL,
    vars jsonb NOT NULL
);
CREATE TABLE variables.event (
    event_id uuid NOT NULL,
    vars jsonb NOT NULL
);
CREATE TABLE variables.team (
    team_id uuid NOT NULL,
    vars jsonb NOT NULL
);
ALTER TABLE ONLY challenges.active
    ADD CONSTRAINT active_pkey PRIMARY KEY (event_id, challenge_id);
ALTER TABLE ONLY challenges.challenges
    ADD CONSTRAINT challenges_name_key UNIQUE (name);
ALTER TABLE ONLY challenges.challenges
    ADD CONSTRAINT challenges_pkey PRIMARY KEY (id);
ALTER TABLE ONLY challenges.files
    ADD CONSTRAINT files_pkey PRIMARY KEY (event_id, challenge_id, file_id);
ALTER TABLE ONLY challenges.flags
    ADD CONSTRAINT flags_pkey PRIMARY KEY (event_id, challenge_id, flag);
ALTER TABLE ONLY challenges.submissions
    ADD CONSTRAINT submissions_pkey PRIMARY KEY (challenge_id, team_id, flag, event_id);
ALTER TABLE ONLY challenges.tags
    ADD CONSTRAINT tags_pkey PRIMARY KEY (challenge_id, tag, scope);
ALTER TABLE ONLY checks.adversarial_status
    ADD CONSTRAINT adversarial_status_pkey PRIMARY KEY (event_id, check_id, team_id);
ALTER TABLE ONLY checks.checks
    ADD CONSTRAINT checks_name_key UNIQUE (name);
ALTER TABLE ONLY checks.checks
    ADD CONSTRAINT checks_pkey PRIMARY KEY (id);
ALTER TABLE ONLY checks.deployed
    ADD CONSTRAINT deployed_id_key UNIQUE (id);
ALTER TABLE ONLY checks.deployed
    ADD CONSTRAINT deployed_pkey PRIMARY KEY (check_id, event_id, team_id);
ALTER TABLE ONLY checks.results
    ADD CONSTRAINT results_pkey PRIMARY KEY (id);
ALTER TABLE ONLY checks.service_status
    ADD CONSTRAINT service_status_pkey PRIMARY KEY (event_id, check_id, team_id);
ALTER TABLE ONLY public.coaches
    ADD CONSTRAINT coaches_pkey PRIMARY KEY (team_id, user_id);
ALTER TABLE ONLY public.environments
    ADD CONSTRAINT environment_name_key UNIQUE (name);
ALTER TABLE ONLY public.environments
    ADD CONSTRAINT environment_pkey PRIMARY KEY (id);
ALTER TABLE ONLY public.environment_settings
    ADD CONSTRAINT environment_settings_pkey PRIMARY KEY (environment_id);
ALTER TABLE ONLY public.event_group_event
    ADD CONSTRAINT event_group_event_pkey PRIMARY KEY (event_group_id, event_id);
ALTER TABLE ONLY public.event_groups
    ADD CONSTRAINT event_groups_name_key UNIQUE (name);
ALTER TABLE ONLY public.event_groups
    ADD CONSTRAINT event_groups_pkey PRIMARY KEY (id);
ALTER TABLE ONLY public.event_registration
    ADD CONSTRAINT event_registration_pkey PRIMARY KEY (event_id, team_id);
ALTER TABLE ONLY public.event_registration_settings
    ADD CONSTRAINT event_registration_settings_pkey PRIMARY KEY (event_id);
ALTER TABLE ONLY public.events
    ADD CONSTRAINT events_name_key UNIQUE (name);
ALTER TABLE ONLY public.events
    ADD CONSTRAINT events_pkey PRIMARY KEY (id);
ALTER TABLE ONLY public.institution_domains
    ADD CONSTRAINT institution_domains_pkey PRIMARY KEY (institution_id, domain);
ALTER TABLE ONLY public.institution_region
    ADD CONSTRAINT institution_region_pkey PRIMARY KEY (institution_id, region_id);
ALTER TABLE ONLY public.institutions
    ADD CONSTRAINT institutions_name_key UNIQUE (name);
ALTER TABLE ONLY public.institutions
    ADD CONSTRAINT institutions_pkey PRIMARY KEY (id);
ALTER TABLE ONLY public.regions
    ADD CONSTRAINT regions_name_key UNIQUE (name);
ALTER TABLE ONLY public.regions
    ADD CONSTRAINT regions_pkey PRIMARY KEY (id);
ALTER TABLE ONLY public.team_membership
    ADD CONSTRAINT team_membership_pkey PRIMARY KEY (team_id, user_id);
ALTER TABLE ONLY public.teams
    ADD CONSTRAINT teams_pkey PRIMARY KEY (id);
ALTER TABLE ONLY public.teams
    ADD CONSTRAINT teams_tapi_id_key UNIQUE (tapi_id);
ALTER TABLE ONLY scoreboard.displayed_services
    ADD CONSTRAINT displayed_services_order_key UNIQUE ("order");
ALTER TABLE ONLY scoreboard.displayed_services
    ADD CONSTRAINT displayed_services_pkey PRIMARY KEY (event_id, check_id);
ALTER TABLE ONLY scoreboard.event_settings
    ADD CONSTRAINT event_settings_pkey PRIMARY KEY (event_id);
ALTER TABLE ONLY scoreboard.kanban
    ADD CONSTRAINT kanban_pkey PRIMARY KEY (id);
ALTER TABLE ONLY scoreboard.team_settings
    ADD CONSTRAINT team_settings_order_key UNIQUE ("order");
ALTER TABLE ONLY scoreboard.team_settings
    ADD CONSTRAINT team_settings_pkey PRIMARY KEY (event_id, team_id);
ALTER TABLE ONLY scoring.bonus_points
    ADD CONSTRAINT bonus_points_pkey PRIMARY KEY (id);
ALTER TABLE ONLY scoring.periods
    ADD CONSTRAINT periods_pkey PRIMARY KEY (id);
ALTER TABLE ONLY scoring.service_period_score
    ADD CONSTRAINT service_period_score_pkey PRIMARY KEY (event_id, check_id, team_id, period_id);
ALTER TABLE ONLY scoring.points
    ADD CONSTRAINT service_points_pkey PRIMARY KEY (period_id, check_id);
ALTER TABLE ONLY storage.buckets
    ADD CONSTRAINT buckets_pkey PRIMARY KEY (id);
ALTER TABLE ONLY storage.files
    ADD CONSTRAINT files_pkey PRIMARY KEY (id);
ALTER TABLE ONLY storage.schema_migrations
    ADD CONSTRAINT schema_migrations_pkey PRIMARY KEY (version);
ALTER TABLE ONLY variables."check"
    ADD CONSTRAINT check_pkey PRIMARY KEY (check_id);
ALTER TABLE ONLY variables.deployment
    ADD CONSTRAINT deployment_pkey PRIMARY KEY (deploy_id);
ALTER TABLE ONLY variables.event
    ADD CONSTRAINT event_pkey PRIMARY KEY (event_id);
ALTER TABLE ONLY variables.team
    ADD CONSTRAINT team_pkey PRIMARY KEY (team_id);
CREATE INDEX "challenges.active.challenge_id.single_idx" ON challenges.active USING hash (challenge_id);
CREATE INDEX "challenges.active.event_id.single_idx" ON challenges.active USING hash (event_id);
CREATE INDEX "challenges.active.release_time.single_idx" ON challenges.active USING btree (release_time);
CREATE INDEX "challenges.files.challenge_id.single_idx" ON challenges.files USING hash (challenge_id);
CREATE INDEX "challenges.files.event_id.single_idx" ON challenges.files USING hash (event_id);
CREATE INDEX "challenges.flags.challenge_id.single_idx" ON challenges.flags USING hash (challenge_id);
CREATE INDEX "challenges.flags.event_id.single_idx" ON challenges.flags USING hash (event_id);
CREATE INDEX "challenges.submissions.challenge_id.single_idx" ON challenges.submissions USING hash (challenge_id);
CREATE INDEX "challenges.submissions.event_id.single_idx" ON challenges.submissions USING hash (event_id);
CREATE INDEX "challenges.submissions.flag.single_idx" ON challenges.submissions USING btree (flag);
CREATE INDEX "challenges.submissions.team_id.single_idx" ON challenges.submissions USING hash (team_id);
CREATE INDEX "challenges.submissions.time.single_idx" ON challenges.submissions USING btree ("time");
CREATE INDEX "challenges.tags.by_scope" ON challenges.tags USING btree (scope);
CREATE INDEX "challenges.tags.by_tag" ON challenges.tags USING btree (tag);
CREATE INDEX "checks.adversarial_status.check_id.single_idx" ON checks.adversarial_status USING hash (check_id);
CREATE INDEX "checks.adversarial_status.event_id.single_idx" ON checks.adversarial_status USING hash (event_id);
CREATE INDEX "checks.adversarial_status.team_id.single_idx" ON checks.adversarial_status USING hash (team_id);
CREATE INDEX "checks.checks.type.single_idx" ON checks.checks USING btree (type);
CREATE INDEX "checks.results.check_id.single_idx" ON checks.results USING hash (check_id);
CREATE INDEX "checks.results.check_time.single_idx" ON checks.results USING btree (check_time);
CREATE INDEX "checks.results.event_id,team_id,check_id.composite_idx" ON checks.results USING btree (event_id, team_id, check_id);
CREATE INDEX "checks.results.event_id.single_idx" ON checks.results USING hash (event_id);
CREATE INDEX "checks.results.result_code.single_idx" ON checks.results USING btree (result_code);
CREATE INDEX "checks.results.team_id.single_idx" ON checks.results USING hash (team_id);
CREATE INDEX "checks.service_status.check_id.single_idx" ON checks.service_status USING hash (check_id);
CREATE INDEX "checks.service_status.event_id.single_idx" ON checks.service_status USING hash (event_id);
CREATE INDEX "checks.service_status.team_id.single_idx" ON checks.service_status USING hash (team_id);
CREATE INDEX "public.event_registration.event_id.single_idx" ON public.event_registration USING hash (event_id);
CREATE INDEX "public.event_registration.team_id.single_idx" ON public.event_registration USING hash (team_id);
CREATE INDEX "public.team_membership.team_id.single_idx" ON public.team_membership USING hash (team_id);
CREATE INDEX "public.team_membership.user_id.single_idx" ON public.team_membership USING hash (user_id);
CREATE INDEX "scoreboard.displayed_services.check_id.single_idx" ON scoreboard.displayed_services USING hash (check_id);
CREATE INDEX "scoreboard.displayed_services.event_id.single_idx" ON scoreboard.displayed_services USING hash (event_id);
CREATE INDEX "scoreboard.team_settings.event_id.single_idx" ON scoreboard.team_settings USING hash (event_id);
CREATE INDEX "scoreboard.team_settings.team_id.single_idx" ON scoreboard.team_settings USING hash (team_id);
CREATE INDEX "scoring.bonus_points.event_id.single_idx" ON scoring.bonus_points USING hash (event_id);
CREATE INDEX "scoring.bonus_points.team_id.single_idx" ON scoring.bonus_points USING hash (team_id);
CREATE INDEX "scoring.periods.event_id.single_idx" ON scoring.periods USING hash (event_id);
CREATE INDEX "scoring.points.check_id.single_idx" ON scoring.points USING hash (check_id);
CREATE INDEX "scoring.points.period_id.single_idx" ON scoring.points USING hash (period_id);
CREATE INDEX "scoring.service_period_score.check_id.single_idx" ON scoring.service_period_score USING hash (check_id);
CREATE INDEX "scoring.service_period_score.event_id.single_idx" ON scoring.service_period_score USING hash (event_id);
CREATE INDEX "scoring.service_period_score.event_team_check.multi_idx" ON scoring.service_period_score USING btree (event_id, team_id, check_id);
CREATE INDEX "scoring.service_period_score.period_check.multi_idx" ON scoring.service_period_score USING btree (period_id, check_id);
CREATE INDEX "scoring.service_period_score.period_id.single_idx" ON scoring.service_period_score USING hash (period_id);
CREATE INDEX "scoring.service_period_score.team_id.single_idx" ON scoring.service_period_score USING hash (team_id);
CREATE OR REPLACE VIEW scoreboard.challenge_solves_by_category AS
 WITH all_teams AS (
         SELECT ts.event_id,
            ts.team_id,
            ts.visible,
            cac.category,
            cac.count AS total
           FROM (scoreboard.team_settings ts
             JOIN ( SELECT ca.event_id,
                    c.category,
                    count(c.*) AS count
                   FROM (challenges.active ca
                     JOIN ( SELECT challenges.id,
                            challenges.category
                           FROM challenges.challenges) c ON ((c.id = ca.challenge_id)))
                  GROUP BY ca.event_id, c.category) cac ON ((cac.event_id = ts.event_id)))
          GROUP BY ts.event_id, ts.team_id, cac.category, cac.count
        )
 SELECT allt.event_id,
    allt.team_id,
    allt.visible,
    allt.category,
    count(allt.cat) AS solved,
    allt.total
   FROM (all_teams
     LEFT JOIN ( SELECT cs.event_id AS eid,
            cs.team_id AS tid,
            cs.challenge_id,
            cs.category AS cat
           FROM (challenges.solves
             JOIN ( SELECT challenges.id,
                    challenges.category
                   FROM challenges.challenges) cc ON ((cc.id = solves.challenge_id))) cs) css ON (((css.tid = all_teams.team_id) AND (css.eid = all_teams.event_id) AND (css.cat = all_teams.category)))) allt
  GROUP BY allt.event_id, allt.team_id, allt.visible, allt.category, allt.total
  ORDER BY allt.team_id, allt.category;
CREATE TRIGGER trigger_refresh_ctf_views AFTER INSERT OR DELETE OR UPDATE ON challenges.active FOR EACH STATEMENT EXECUTE FUNCTION public.refresh_ctf_views();
CREATE TRIGGER trigger_refresh_ctf_views AFTER INSERT OR DELETE OR UPDATE ON challenges.challenges FOR EACH STATEMENT EXECUTE FUNCTION public.refresh_ctf_views();
CREATE TRIGGER trigger_refresh_ctf_views AFTER INSERT OR DELETE OR UPDATE ON challenges.flags FOR EACH STATEMENT EXECUTE FUNCTION public.refresh_ctf_views();
CREATE TRIGGER trigger_refresh_ctf_views AFTER INSERT OR DELETE OR UPDATE ON challenges.submissions FOR EACH STATEMENT EXECUTE FUNCTION public.refresh_ctf_views();
CREATE TRIGGER trigger_row_update_adversarial_status AFTER INSERT ON checks.results FOR EACH ROW EXECUTE FUNCTION checks.row_update_adversarial_status();
CREATE TRIGGER trigger_row_update_period_scores AFTER INSERT ON checks.results FOR EACH ROW EXECUTE FUNCTION scoring.row_update_period_scores();
CREATE TRIGGER trigger_row_update_service_status AFTER INSERT ON checks.results FOR EACH ROW EXECUTE FUNCTION checks.row_update_service_status();
CREATE TRIGGER insert_event_registration_validate_event_group_trigger BEFORE INSERT ON public.event_registration FOR EACH ROW EXECUTE FUNCTION public.insert_event_registration_validate_event_group();
CREATE TRIGGER insert_event_registration_validate_region_trigger BEFORE INSERT ON public.event_registration FOR EACH ROW EXECUTE FUNCTION public.insert_event_registration_validate_region();
CREATE TRIGGER trigger_adjust_period_points AFTER DELETE OR UPDATE ON scoring.periods FOR EACH ROW EXECUTE FUNCTION scoring.adjust_period_points();
CREATE TRIGGER trigger_points_calculate_period_scores AFTER INSERT OR DELETE OR UPDATE ON scoring.points FOR EACH ROW EXECUTE FUNCTION scoring.calculate_period_scores();
CREATE TRIGGER trigger_refresh_bonus_score_view AFTER INSERT OR DELETE OR UPDATE ON scoring.bonus_points FOR EACH ROW EXECUTE FUNCTION scoring.refresh_bonus_score_view();
CREATE TRIGGER check_default_bucket_delete BEFORE DELETE ON storage.buckets FOR EACH ROW EXECUTE FUNCTION storage.protect_default_bucket_delete();
CREATE TRIGGER check_default_bucket_update BEFORE UPDATE ON storage.buckets FOR EACH ROW EXECUTE FUNCTION storage.protect_default_bucket_update();
CREATE TRIGGER set_storage_buckets_updated_at BEFORE UPDATE ON storage.buckets FOR EACH ROW EXECUTE FUNCTION storage.set_current_timestamp_updated_at();
CREATE TRIGGER set_storage_files_updated_at BEFORE UPDATE ON storage.files FOR EACH ROW EXECUTE FUNCTION storage.set_current_timestamp_updated_at();
ALTER TABLE ONLY challenges.active
    ADD CONSTRAINT active_challenge_id_fkey FOREIGN KEY (challenge_id) REFERENCES challenges.challenges(id) ON UPDATE CASCADE ON DELETE CASCADE;
ALTER TABLE ONLY challenges.active
    ADD CONSTRAINT active_event_id_fkey FOREIGN KEY (event_id) REFERENCES public.events(id) ON UPDATE CASCADE ON DELETE CASCADE;
ALTER TABLE ONLY challenges.files
    ADD CONSTRAINT files_challenge_id_fkey FOREIGN KEY (challenge_id) REFERENCES challenges.challenges(id) ON UPDATE CASCADE ON DELETE CASCADE;
ALTER TABLE ONLY challenges.files
    ADD CONSTRAINT files_event_id_fkey FOREIGN KEY (event_id) REFERENCES public.events(id) ON UPDATE CASCADE ON DELETE CASCADE;
ALTER TABLE ONLY challenges.flags
    ADD CONSTRAINT flags_challenge_id_fkey FOREIGN KEY (challenge_id) REFERENCES challenges.challenges(id) ON UPDATE CASCADE ON DELETE CASCADE;
ALTER TABLE ONLY challenges.flags
    ADD CONSTRAINT flags_event_id_fkey FOREIGN KEY (event_id) REFERENCES public.events(id) ON UPDATE CASCADE ON DELETE CASCADE;
ALTER TABLE ONLY challenges.submissions
    ADD CONSTRAINT submissions_challenge_id_fkey FOREIGN KEY (challenge_id) REFERENCES challenges.challenges(id) ON UPDATE CASCADE ON DELETE CASCADE;
ALTER TABLE ONLY challenges.submissions
    ADD CONSTRAINT submissions_event_id_fkey FOREIGN KEY (event_id) REFERENCES public.events(id) ON UPDATE CASCADE ON DELETE CASCADE;
ALTER TABLE ONLY challenges.submissions
    ADD CONSTRAINT submissions_team_id_fkey FOREIGN KEY (team_id) REFERENCES public.teams(id) ON UPDATE CASCADE ON DELETE CASCADE;
ALTER TABLE ONLY challenges.tags
    ADD CONSTRAINT tags_challenge_id_fkey FOREIGN KEY (challenge_id) REFERENCES challenges.challenges(id) ON UPDATE CASCADE ON DELETE CASCADE;
ALTER TABLE ONLY checks.adversarial_status
    ADD CONSTRAINT adversarial_status_check_id_fkey FOREIGN KEY (check_id) REFERENCES checks.checks(id) ON UPDATE CASCADE ON DELETE CASCADE;
ALTER TABLE ONLY checks.adversarial_status
    ADD CONSTRAINT adversarial_status_event_id_fkey FOREIGN KEY (event_id) REFERENCES public.events(id) ON UPDATE CASCADE ON DELETE CASCADE;
ALTER TABLE ONLY checks.adversarial_status
    ADD CONSTRAINT adversarial_status_team_id_fkey FOREIGN KEY (team_id) REFERENCES public.teams(id) ON UPDATE CASCADE ON DELETE CASCADE;
ALTER TABLE ONLY checks.deployed
    ADD CONSTRAINT deployed_check_id_fkey FOREIGN KEY (check_id) REFERENCES checks.checks(id) ON UPDATE CASCADE ON DELETE CASCADE;
ALTER TABLE ONLY checks.deployed
    ADD CONSTRAINT deployed_event_id_fkey FOREIGN KEY (event_id) REFERENCES public.events(id) ON UPDATE CASCADE ON DELETE CASCADE;
ALTER TABLE ONLY checks.deployed
    ADD CONSTRAINT deployed_team_id_fkey FOREIGN KEY (team_id) REFERENCES public.teams(id) ON UPDATE CASCADE ON DELETE CASCADE;
ALTER TABLE ONLY checks.results
    ADD CONSTRAINT results_check_id_fkey FOREIGN KEY (check_id) REFERENCES checks.checks(id) ON UPDATE CASCADE ON DELETE CASCADE;
ALTER TABLE ONLY checks.results
    ADD CONSTRAINT results_event_id_fkey FOREIGN KEY (event_id) REFERENCES public.events(id) ON UPDATE CASCADE ON DELETE CASCADE;
ALTER TABLE ONLY checks.results
    ADD CONSTRAINT results_team_id_fkey FOREIGN KEY (team_id) REFERENCES public.teams(id) ON UPDATE CASCADE ON DELETE CASCADE;
ALTER TABLE ONLY checks.service_status
    ADD CONSTRAINT service_status_check_id_fkey FOREIGN KEY (check_id) REFERENCES checks.checks(id) ON UPDATE CASCADE ON DELETE CASCADE;
ALTER TABLE ONLY checks.service_status
    ADD CONSTRAINT service_status_event_id_fkey FOREIGN KEY (event_id) REFERENCES public.events(id) ON UPDATE CASCADE ON DELETE CASCADE;
ALTER TABLE ONLY checks.service_status
    ADD CONSTRAINT service_status_team_id_fkey FOREIGN KEY (team_id) REFERENCES public.teams(id) ON UPDATE CASCADE ON DELETE CASCADE;
ALTER TABLE ONLY public.coaches
    ADD CONSTRAINT coaches_team_id_fkey FOREIGN KEY (team_id) REFERENCES public.teams(id) ON UPDATE CASCADE ON DELETE CASCADE;
ALTER TABLE ONLY public.environments
    ADD CONSTRAINT environment_event_id_fkey FOREIGN KEY (event_id) REFERENCES public.events(id) ON UPDATE CASCADE ON DELETE CASCADE;
ALTER TABLE ONLY public.environment_settings
    ADD CONSTRAINT environment_settings_environment_id_fkey FOREIGN KEY (environment_id) REFERENCES public.environments(id) ON UPDATE CASCADE ON DELETE CASCADE;
ALTER TABLE ONLY public.event_group_event
    ADD CONSTRAINT event_group_event_event_group_id_fkey FOREIGN KEY (event_group_id) REFERENCES public.event_groups(id) ON UPDATE CASCADE ON DELETE CASCADE;
ALTER TABLE ONLY public.event_group_event
    ADD CONSTRAINT event_group_event_event_id_fkey FOREIGN KEY (event_id) REFERENCES public.events(id) ON UPDATE CASCADE ON DELETE CASCADE;
ALTER TABLE ONLY public.event_registration
    ADD CONSTRAINT event_registration_event_id_fkey FOREIGN KEY (event_id) REFERENCES public.events(id) ON UPDATE CASCADE ON DELETE CASCADE;
ALTER TABLE ONLY public.event_registration_settings
    ADD CONSTRAINT event_registration_settings_event_id_fkey FOREIGN KEY (event_id) REFERENCES public.events(id) ON UPDATE CASCADE ON DELETE CASCADE;
ALTER TABLE ONLY public.event_registration_settings
    ADD CONSTRAINT event_registration_settings_region_id_fkey FOREIGN KEY (region_id) REFERENCES public.regions(id) ON UPDATE CASCADE ON DELETE CASCADE;
ALTER TABLE ONLY public.event_registration
    ADD CONSTRAINT event_registration_team_id_fkey FOREIGN KEY (team_id) REFERENCES public.teams(id) ON UPDATE CASCADE ON DELETE CASCADE;
ALTER TABLE ONLY public.institution_domains
    ADD CONSTRAINT institution_domains_institution_id_fkey FOREIGN KEY (institution_id) REFERENCES public.institutions(id) ON UPDATE CASCADE ON DELETE CASCADE;
ALTER TABLE ONLY public.institution_region
    ADD CONSTRAINT institution_region_institution_id_fkey FOREIGN KEY (institution_id) REFERENCES public.institutions(id) ON UPDATE CASCADE ON DELETE CASCADE;
ALTER TABLE ONLY public.institution_region
    ADD CONSTRAINT institution_region_region_id_fkey FOREIGN KEY (region_id) REFERENCES public.regions(id) ON UPDATE CASCADE ON DELETE CASCADE;
ALTER TABLE ONLY public.team_membership
    ADD CONSTRAINT team_membership_team_id_fkey FOREIGN KEY (team_id) REFERENCES public.teams(id) ON UPDATE CASCADE ON DELETE CASCADE;
ALTER TABLE ONLY public.teams
    ADD CONSTRAINT teams_institution_id_fkey FOREIGN KEY (institution_id) REFERENCES public.institutions(id) ON UPDATE CASCADE ON DELETE CASCADE;
ALTER TABLE ONLY scoreboard.displayed_services
    ADD CONSTRAINT displayed_services_check_id_fkey FOREIGN KEY (check_id) REFERENCES checks.checks(id) ON UPDATE CASCADE ON DELETE CASCADE;
ALTER TABLE ONLY scoreboard.displayed_services
    ADD CONSTRAINT displayed_services_event_id_fkey FOREIGN KEY (event_id) REFERENCES public.events(id) ON UPDATE CASCADE ON DELETE CASCADE;
ALTER TABLE ONLY scoreboard.event_settings
    ADD CONSTRAINT event_settings_event_id_fkey FOREIGN KEY (event_id) REFERENCES public.events(id) ON UPDATE CASCADE ON DELETE CASCADE;
ALTER TABLE ONLY scoreboard.team_settings
    ADD CONSTRAINT team_settings_event_id_fkey FOREIGN KEY (event_id) REFERENCES public.events(id) ON UPDATE CASCADE ON DELETE CASCADE;
ALTER TABLE ONLY scoreboard.team_settings
    ADD CONSTRAINT team_settings_team_id_fkey FOREIGN KEY (team_id) REFERENCES public.teams(id) ON UPDATE CASCADE ON DELETE CASCADE;
ALTER TABLE ONLY scoring.bonus_points
    ADD CONSTRAINT bonus_points_event_id_fkey FOREIGN KEY (event_id) REFERENCES public.events(id) ON UPDATE CASCADE ON DELETE CASCADE;
ALTER TABLE ONLY scoring.bonus_points
    ADD CONSTRAINT bonus_points_team_id_fkey FOREIGN KEY (team_id) REFERENCES public.teams(id) ON UPDATE CASCADE ON DELETE CASCADE;
ALTER TABLE ONLY scoring.periods
    ADD CONSTRAINT periods_event_id_fkey FOREIGN KEY (event_id) REFERENCES public.events(id) ON UPDATE CASCADE ON DELETE CASCADE;
ALTER TABLE ONLY scoring.points
    ADD CONSTRAINT points_check_id_fkey FOREIGN KEY (check_id) REFERENCES checks.checks(id) ON UPDATE CASCADE ON DELETE CASCADE;
ALTER TABLE ONLY scoring.points
    ADD CONSTRAINT points_period_id_fkey FOREIGN KEY (period_id) REFERENCES scoring.periods(id) ON UPDATE CASCADE ON DELETE CASCADE;
ALTER TABLE ONLY storage.files
    ADD CONSTRAINT fk_bucket FOREIGN KEY (bucket_id) REFERENCES storage.buckets(id) ON UPDATE CASCADE ON DELETE CASCADE;
ALTER TABLE ONLY variables."check"
    ADD CONSTRAINT check_check_id_fkey FOREIGN KEY (check_id) REFERENCES checks.checks(id) ON UPDATE CASCADE ON DELETE CASCADE;
ALTER TABLE ONLY variables.deployment
    ADD CONSTRAINT deployment_deploy_id_fkey FOREIGN KEY (deploy_id) REFERENCES checks.deployed(id) ON UPDATE CASCADE ON DELETE CASCADE;
ALTER TABLE ONLY variables.event
    ADD CONSTRAINT event_event_id_fkey FOREIGN KEY (event_id) REFERENCES public.events(id) ON UPDATE CASCADE ON DELETE CASCADE;
ALTER TABLE ONLY variables.team
    ADD CONSTRAINT team_team_id_fkey FOREIGN KEY (team_id) REFERENCES public.teams(id) ON UPDATE CASCADE ON DELETE CASCADE;
