alter table "checks"."checks" drop column IF EXISTS "details" cascade;
alter table "public"."environments" drop column IF EXISTS "topology" cascade;
alter table "public"."environments" drop column IF EXISTS "scoring_details" cascade;
alter table "public"."environments" drop column IF EXISTS "info" cascade;
