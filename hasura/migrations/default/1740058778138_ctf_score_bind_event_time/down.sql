DROP VIEW "scoreboard"."score";
DROP VIEW "scoring"."total_score";
DROP MATERIALIZED VIEW "scoring"."ctf_score";
CREATE MATERIALIZED VIEW "scoring"."ctf_score" AS
SELECT
  s.event_id,
  s.team_id,
  sum(r.points) AS score,
  count(r.*) AS solves
FROM
  (
    challenges.solves s
    JOIN (
      SELECT
        a.event_id,
        a.challenge_id,
        a.points
      FROM
        challenges.active a
    ) r ON (
      (
        (s.event_id = r.event_id)
        AND (s.challenge_id = r.challenge_id)
      )
    )
  )
GROUP BY
  s.event_id,
  s.team_id;
CREATE
OR REPLACE VIEW "scoring"."total_score" AS WITH summary AS (
  WITH entries AS (
    SELECT
      bonus_score.event_id,
      bonus_score.team_id,
      bonus_score.score AS bonus_score,
      0 AS ctf_score,
      0 AS service_score
    FROM
      scoring.bonus_score
    UNION ALL
    SELECT
      ctf_score.event_id,
      ctf_score.team_id,
      0 AS bonus_score,
      ctf_score.score AS ctf_score,
      0 AS service_score
    FROM
      scoring.ctf_score
    UNION ALL
    SELECT
      service_score.event_id,
      service_score.team_id,
      0 AS bonus_score,
      0 AS ctf_score,
      service_score.score AS service_score
    FROM
      scoring.service_score
  )
  SELECT
    entries.event_id,
    entries.team_id,
    sum(entries.bonus_score) AS bonus,
    sum(entries.ctf_score) AS ctf,
    sum(entries.service_score) AS service
  FROM
    entries
  GROUP BY
    entries.event_id,
    entries.team_id
)
SELECT
  summary.event_id,
  summary.team_id,
  summary.bonus,
  summary.ctf,
  summary.service,
  ((summary.bonus + summary.ctf) + summary.service) AS total
FROM
  summary;
CREATE
OR REPLACE VIEW "scoreboard"."score" AS
SELECT
  ts.event_id,
  ts.team_id,
  COALESCE(score.bonus, (0) :: real) AS bonus,
  COALESCE(score.ctf, (0) :: real) AS ctf,
  COALESCE(
    (score.service) :: double precision,
    (0) :: double precision
  ) AS service,
  COALESCE(
    (score.total) :: double precision,
    (0) :: double precision
  ) AS total,
  ts.visible
FROM
  (
    scoreboard.team_settings ts
    LEFT JOIN (
      SELECT
        total_score.event_id,
        total_score.team_id,
        total_score.bonus,
        total_score.ctf,
        total_score.service,
        total_score.total
      FROM
        scoring.total_score
    ) score ON (
      (
        (score.team_id = ts.team_id)
        AND (score.event_id = ts.event_id)
      )
    )
  );
