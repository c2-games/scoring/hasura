alter table "wrapup"."snapshots_service" drop constraint "snapshots_service_event_id_team_id_snapshot_id_fkey",
  add constraint "snapshots_service_event_id_team_id_snapshot_id_fkey"
  foreign key ("team_id")
  references "public"."teams"
  ("id") on update cascade on delete restrict;
