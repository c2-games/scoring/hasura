alter table "wrapup"."snapshots_bonus" drop constraint "snapshots_bonus_snapshot_id_fkey",
  add constraint "snapshots_bonus_snapshot_id_fkey"
  foreign key ("snapshot_id")
  references "wrapup"."snapshots"
  ("id") on update cascade on delete cascade;
