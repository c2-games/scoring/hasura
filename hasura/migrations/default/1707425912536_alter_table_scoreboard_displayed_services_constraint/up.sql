alter table "scoreboard"."displayed_services" drop constraint "displayed_services_order_key";
alter table "scoreboard"."displayed_services" add constraint "displayed_services_order_event_id_key" unique ("order", "event_id");
