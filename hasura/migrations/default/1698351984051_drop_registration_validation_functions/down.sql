-- Could not auto-generate a down migration.
-- Please write an appropriate down migration for the SQL below:

-- recreate functions
CREATE FUNCTION public.insert_event_registration_validate_event_group() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
BEGIN
    IF (
        WITH settings AS (
            SELECT
                NEW.event_id,
                eg.*,
                (SELECT COUNT(*) FROM event_registration er WHERE ege.event_id = er.event_id) AS registered_teams,
                (SELECT COUNT(*) FROM (event_registration er INNER JOIN teams t ON er.team_id = t.id) WHERE team.institution_id = t.institution_id) AS institution_teams,
                (SELECT COUNT(*) FROM (event_registration er INNER JOIN teams t ON er.team_id = t.id) WHERE team.institution_id = t.institution_id AND er.event_id = NEW.event_id) AS event_institution_teams,
                (
                    SELECT COUNT(*)
                    FROM (
                        event_registration er
                        INNER JOIN teams t ON er.team_id = t.id
                        INNER JOIN event_group_event iege ON er.event_id = iege.event_id
                        INNER JOIN event_groups ieg ON ieg.id = iege.event_group_id
                    ) WHERE ieg.id = ege.event_group_id AND team.institution_id = t.institution_id AND er.event_id <> NEW.event_id
                ) AS other_event_institution_teams
            FROM (event_group_event ege
                INNER JOIN event_groups eg ON eg.id = ege.event_group_id
                INNER JOIN teams team ON team.id = NEW.team_id
            )
            WHERE ege.event_id = NEW.event_id
        )
        -- Check if this is a valid registration when compared to the event group settings
        SELECT (
            -- If there is still room for teams on this event
            registered_teams < max_teams_per_event
            -- If there is still room for this institution to register teams for this event
            AND event_institution_teams < max_institution_teams_per_event
            -- If all teams from this institution, only have teams registered for this event (if this setting is toggled)
            AND ((NOT institution_teams_same_event) OR other_event_institution_teams < 1)
        )
        FROM settings
        WHERE event_id = NEW.event_id
        LIMIT 1
    ) THEN
        RETURN NEW;
    END IF;
    -- Do not perform the insert if checks fail
    RETURN NULL;
END;
$$;

CREATE FUNCTION public.insert_event_registration_validate_region() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
DECLARE
    team_regions uuid[];
    event_region uuid;
BEGIN
    -- Get the event region
    SELECT
        region_id INTO event_region
    FROM event_registration_settings
    WHERE event_id = NEW.event_id;
    -- If the event_region is not set, allow any team to sign up
    IF (event_region IS NULL) THEN RETURN NEW; END IF;
    -- Get a list of regions related to this team
    SELECT
        array(
            SELECT
                region_id
            FROM institution_region
            WHERE t.institution_id = institution_id
        ) INTO team_regions
    FROM teams t
    WHERE t.id = NEW.team_id;
    -- If the event_region is in the list of team regions, then allow signup
    IF (SELECT event_region = ANY (team_regions::uuid[])) THEN
        RETURN NEW;
    ELSE
        RETURN NULL;
    END IF;
END;
$$;

CREATE OR REPLACE FUNCTION public.roster_change_allowed(teams_row public.teams) RETURNS boolean
    LANGUAGE plpgsql IMMUTABLE
    AS $$
BEGIN
    -- If this team is not registered to any events, then it may make changes
    IF (NOT EXISTS(SELECT * FROM event_registration WHERE team_id = teams_row.id)) THEN
        RETURN true;
    END IF;
    RETURN COALESCE((
        SELECT
            (SELECT roster_change_close FROM event_registration_settings WHERE event_id = er.event_id AND registration_open < now()) AS roster_change_close
        FROM (event_registration er
            JOIN (
                SELECT
                    id,
                    start,
                    "end"
                FROM events
            ) e ON e.id = er.event_id
        )
        WHERE team_id = teams_row.id AND now() < e.start
        ORDER BY roster_change_close ASC
        LIMIT 1
    ) > now(), false);
    -- If roster_change_close is null, then do not allow changes
END;
$$;


-- recreate triggers
CREATE TRIGGER insert_event_registration_validate_event_group_trigger BEFORE INSERT ON public.event_registration FOR EACH ROW EXECUTE FUNCTION public.insert_event_registration_validate_event_group();
CREATE TRIGGER insert_event_registration_validate_region_trigger BEFORE INSERT ON public.event_registration FOR EACH ROW EXECUTE FUNCTION public.insert_event_registration_validate_region();

