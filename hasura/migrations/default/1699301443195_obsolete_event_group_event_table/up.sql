
alter table "public"."events" add column "event_group_id" uuid null;

UPDATE public.events
    SET event_group_id = (
        SELECT event_group_id
        FROM event_group_event ege
        WHERE id = ege.event_id LIMIT 1
    );

DROP TRIGGER IF EXISTS insert_event_registration_validate_event_group_trigger ON public.event_registration;
DROP FUNCTION IF EXISTS public.insert_event_registration_validate_event_group();

DROP TABLE IF EXISTS "public"."event_group_event";

alter table "public"."events"
  add constraint "events_event_group_id_fkey"
  foreign key ("event_group_id")
  references "public"."event_groups"
  ("id") on update set null on delete set null;
