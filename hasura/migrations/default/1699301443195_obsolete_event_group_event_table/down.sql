
alter table "public"."events" drop constraint "events_event_group_id_fkey";

CREATE TABLE "public"."event_group_event" (
    "event_group_id" uuid NOT NULL, "event_id" uuid NOT NULL,
    PRIMARY KEY ("event_id", "event_group_id") ,
    FOREIGN KEY ("event_group_id") REFERENCES "public"."event_groups"("id") ON UPDATE cascade ON DELETE cascade,
    FOREIGN KEY ("event_id") REFERENCES "public"."events"("id") ON UPDATE cascade ON DELETE cascade
);

INSERT INTO "public"."event_group_event" (event_group_id, event_id)
SELECT event_group_id, id
FROM "public"."events"
WHERE event_group_id is NOT NULL;

alter table "public"."events" drop column "event_group_id";

CREATE FUNCTION public.insert_event_registration_validate_event_group() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
BEGIN
    IF (
        WITH settings AS (
            SELECT
                NEW.event_id,
                eg.*,
                (SELECT COUNT(*) FROM event_registration er WHERE ege.event_id = er.event_id) AS registered_teams,
                (SELECT COUNT(*) FROM (event_registration er INNER JOIN teams t ON er.team_id = t.id) WHERE team.institution_id = t.institution_id) AS institution_teams,
                (SELECT COUNT(*) FROM (event_registration er INNER JOIN teams t ON er.team_id = t.id) WHERE team.institution_id = t.institution_id AND er.event_id = NEW.event_id) AS event_institution_teams,
                (
                    SELECT COUNT(*)
                    FROM (
                        event_registration er
                        INNER JOIN teams t ON er.team_id = t.id
                        INNER JOIN event_group_event iege ON er.event_id = iege.event_id
                        INNER JOIN event_groups ieg ON ieg.id = iege.event_group_id
                    ) WHERE ieg.id = ege.event_group_id AND team.institution_id = t.institution_id AND er.event_id <> NEW.event_id
                ) AS other_event_institution_teams
            FROM (event_group_event ege
                INNER JOIN event_groups eg ON eg.id = ege.event_group_id
                INNER JOIN teams team ON team.id = NEW.team_id
            )
            WHERE ege.event_id = NEW.event_id
        )
        -- Check if this is a valid registration when compared to the event group settings
        SELECT (
            -- If there is still room for teams on this event
            registered_teams < max_teams_per_event
            -- If there is still room for this institution to register teams for this event
            AND event_institution_teams < max_institution_teams_per_event
            -- If all teams from this institution, only have teams registered for this event (if this setting is toggled)
            AND ((NOT institution_teams_same_event) OR other_event_institution_teams < 1)
        )
        FROM settings
        WHERE event_id = NEW.event_id
        LIMIT 1
    ) THEN
        RETURN NEW;
    END IF;
    -- Do not perform the insert if checks fail
    RETURN NULL;
END;
$$;

CREATE TRIGGER insert_event_registration_validate_event_group_trigger BEFORE INSERT ON public.event_registration FOR EACH ROW EXECUTE FUNCTION public.insert_event_registration_validate_event_group();
