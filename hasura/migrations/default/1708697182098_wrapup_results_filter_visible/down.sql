CREATE OR REPLACE VIEW "wrapup"."results" AS
 WITH recent AS (
         SELECT max(ws."time") AS "time",
            ws.event_id,
            ws.team_id,
            ( SELECT max(s_1.total_score) AS max
                   FROM wrapup.snapshots s_1
                  WHERE (s_1.event_id = ws.event_id)) AS top_score
           FROM wrapup.snapshots ws
          GROUP BY ws.event_id, ws.team_id
          ORDER BY (max(ws."time")) DESC
        )
 SELECT s.id AS snapshot_id,
    s.event_id,
    s.team_id AS top_score,
    s.total_score AS score
   FROM (wrapup.snapshots s
     JOIN recent r ON (((r."time" = s."time") AND (r.event_id = s.event_id) AND (r.team_id = s.team_id) AND (s.total_score = r.top_score))));