CREATE OR REPLACE FUNCTION public.roster_change_allowed(teams_row public.teams) RETURNS boolean
    LANGUAGE plpgsql IMMUTABLE
    AS $$
BEGIN
    -- If this team is not registered to any events, then it may make changes
    IF (NOT EXISTS(SELECT * FROM event_registration WHERE team_id = teams_row.id)) THEN
        RETURN true;
    END IF;
    RETURN COALESCE((
        SELECT
            (SELECT roster_change_close FROM event_registration_settings WHERE event_id = er.event_id AND registration_open < now()) AS roster_change_close
        FROM (event_registration er
            JOIN (
                SELECT
                    id,
                    start,
                    "end"
                FROM events
            ) e ON e.id = er.event_id
        )
        WHERE team_id = teams_row.id AND now() < e.start
        ORDER BY roster_change_close ASC
        LIMIT 1
    ) > now(), false);
    -- If roster_change_close is null, then do not allow changes
END;
$$;
