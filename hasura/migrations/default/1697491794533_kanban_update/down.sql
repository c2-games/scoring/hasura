DROP TRIGGER trigger_row_update_scoreboard_kanban ON scoreboard.kanban;
DROP FUNCTION scoreboard.kanban_log_change;

alter table "scoreboard"."kanban" alter column "created_by" drop not null;
alter table "scoreboard"."kanban" add column "created_by" text;

DROP TABLE "scoreboard"."kanban_task_log";

alter table "scoreboard"."kanban" alter column "assignee" drop not null;
alter table "scoreboard"."kanban" add column "assignee" text;
