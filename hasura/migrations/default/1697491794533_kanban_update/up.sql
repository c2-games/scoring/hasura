
alter table "scoreboard"."kanban" drop column "assignee" cascade;

alter table "scoreboard"."kanban" add column "assignee_id" uuid
 null;

CREATE TABLE "scoreboard"."kanban_task_log" ("id" uuid NOT NULL DEFAULT gen_random_uuid(), "kanban_task_id" uuid NOT NULL, "old_status" text NOT NULL, "new_status" Text NOT NULL, "timestamp" timestamptz NOT NULL DEFAULT now(), "old_assignee_id" uuid, "new_assignee_id" uuid, PRIMARY KEY ("id") , FOREIGN KEY ("kanban_task_id") REFERENCES "scoreboard"."kanban"("id") ON UPDATE cascade ON DELETE cascade);
CREATE EXTENSION IF NOT EXISTS pgcrypto;

alter table "scoreboard"."kanban" drop column "created_by" cascade;

alter table "scoreboard"."kanban" add column "created_by" uuid
 not null;

CREATE OR REPLACE FUNCTION scoreboard.kanban_log_change() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
BEGIN
    -- We only care about status or assignment changes
    IF ((NEW.status <> OLD.status) OR (NEW.assignee_id <> OLD.assignee_id)) THEN
        INSERT INTO scoreboard.kanban_task_log (
            kanban_task_id,
            old_status,
            new_status,
            old_assignee_id,
            new_assignee_id
        )
        VALUES (
            NEW.id,
            OLD.status,
            NEW.status,
            OLD.assignee_id,
            NEW.assignee_id
        )
        ON CONFLICT ON CONSTRAINT kanban_task_log_pkey
        DO NOTHING;
    END IF;
    RETURN NULL;
END;
$$;
CREATE OR REPLACE TRIGGER trigger_row_update_scoreboard_kanban AFTER UPDATE ON scoreboard.kanban FOR EACH ROW EXECUTE FUNCTION scoreboard.kanban_log_change();
