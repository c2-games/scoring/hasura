alter table "challenges"."submissions" drop column "submitter_id" cascade;
alter table "challenges"."submissions" add column "submitter" text;
CREATE MATERIALIZED VIEW challenges.solves AS
 WITH matches AS (
         SELECT DISTINCT ON (submissions.event_id, submissions.team_id, submissions.challenge_id) submissions.challenge_id,
            submissions.team_id,
            submissions.flag,
            submissions.submitter,
            submissions."time",
            submissions.event_id,
            m.cid,
            m.flag
           FROM (challenges.submissions submissions
             JOIN ( SELECT f.challenge_id AS cid,
                    f.flag,
                    f.event_id
                   FROM challenges.flags f) m ON (((m.cid = submissions.challenge_id) AND (m.event_id = submissions.event_id))))
          WHERE (submissions.flag = m.flag)
        )
 SELECT matches.event_id,
    matches.team_id,
    matches.challenge_id,
    matches.submitter,
    matches."time"
   FROM matches matches(challenge_id, team_id, flag, submitter, "time", event_id, cid, flag_1)
  ORDER BY matches.event_id, matches.team_id, matches.challenge_id
  WITH NO DATA;
CREATE MATERIALIZED VIEW scoring.ctf_score AS
 SELECT s.event_id,
    s.team_id,
    sum(r.points) AS score,
    count(r.*) AS solves
   FROM (challenges.solves s
     JOIN ( SELECT a.event_id,
            a.challenge_id,
            a.points
           FROM challenges.active a) r ON (((s.event_id = r.event_id) AND (s.challenge_id = r.challenge_id))))
  GROUP BY s.event_id, s.team_id
  WITH NO DATA;
CREATE OR REPLACE VIEW scoring.total_score AS
 WITH summary AS (
         WITH entries AS (
                 SELECT bonus_score.event_id,
                    bonus_score.team_id,
                    bonus_score.score AS bonus_score,
                    0 AS ctf_score,
                    0 AS service_score
                   FROM scoring.bonus_score
                UNION ALL
                 SELECT ctf_score.event_id,
                    ctf_score.team_id,
                    0 AS bonus_score,
                    ctf_score.score AS ctf_score,
                    0 AS service_score
                   FROM scoring.ctf_score
                UNION ALL
                 SELECT service_score.event_id,
                    service_score.team_id,
                    0 AS bonus_score,
                    0 AS ctf_score,
                    service_score.score AS service_score
                   FROM scoring.service_score
                )
         SELECT entries.event_id,
            entries.team_id,
            sum(entries.bonus_score) AS bonus,
            sum(entries.ctf_score) AS ctf,
            sum(entries.service_score) AS service
           FROM entries
          GROUP BY entries.event_id, entries.team_id
        )
 SELECT summary.event_id,
    summary.team_id,
    summary.bonus,
    summary.ctf,
    summary.service,
    ((summary.bonus + summary.ctf) + summary.service) AS total
   FROM summary;
CREATE OR REPLACE VIEW scoreboard.score AS
 SELECT ts.event_id,
    ts.team_id,
    COALESCE(score.bonus, (0)::real) AS bonus,
    COALESCE(score.ctf, (0)::real) AS ctf,
    COALESCE((score.service)::double precision, (0)::double precision) AS service,
    COALESCE((score.total)::double precision, (0)::double precision) AS total,
    ts.visible
   FROM (scoreboard.team_settings ts
     LEFT JOIN ( SELECT total_score.event_id,
            total_score.team_id,
            total_score.bonus,
            total_score.ctf,
            total_score.service,
            total_score.total
           FROM scoring.total_score) score ON (((score.team_id = ts.team_id) AND (score.event_id = ts.event_id))));
CREATE OR REPLACE VIEW scoreboard.challenge_solves_by_category AS
 WITH all_teams AS (
         SELECT ts.event_id,
            ts.team_id,
            ts.visible,
            cac.category,
            cac.count AS total
           FROM (scoreboard.team_settings ts
             JOIN ( SELECT ca.event_id,
                    c.category,
                    count(c.*) AS count
                   FROM (challenges.active ca
                     JOIN ( SELECT challenges.id,
                            challenges.category
                           FROM challenges.challenges) c ON ((c.id = ca.challenge_id)))
                  GROUP BY ca.event_id, c.category) cac ON ((cac.event_id = ts.event_id)))
          GROUP BY ts.event_id, ts.team_id, cac.category, cac.count
        )
 SELECT allt.event_id,
    allt.team_id,
    allt.visible,
    allt.category,
    count(allt.cat) AS solved,
    allt.total
   FROM (all_teams
     LEFT JOIN ( SELECT cs.event_id AS eid,
            cs.team_id AS tid,
            cs.challenge_id,
            cs.category AS cat
           FROM (challenges.solves
             JOIN ( SELECT challenges.id,
                    challenges.category
                   FROM challenges.challenges) cc ON ((cc.id = solves.challenge_id))) cs) css ON (((css.tid = all_teams.team_id) AND (css.eid = all_teams.event_id) AND (css.cat = all_teams.category)))) allt
  GROUP BY allt.event_id, allt.team_id, allt.visible, allt.category, allt.total
  ORDER BY allt.team_id, allt.category;
