SET check_function_bodies = false;
INSERT INTO public.event_groups (id, name, max_institution_teams_per_event, max_teams_per_event, institution_teams_same_event) VALUES ('e876414a-5583-4608-b641-64776e5863ee', 'NCAE CyberGames 2024 Regionals', 3, 12, true);
INSERT INTO public.event_groups (id, name, max_institution_teams_per_event, max_teams_per_event, institution_teams_same_event) VALUES ('63d1dfdd-cf33-4ae5-a2c1-e2dcebe57fd0', 'NCAE CyberGames 2024 Invitational', 1, 12, false);
INSERT INTO public.event_groups (id, name, max_institution_teams_per_event, max_teams_per_event, institution_teams_same_event) VALUES ('55b294c0-c76a-4d03-802e-1b6055c5dfdf', 'dev events', 3, 12, false);
