SET check_function_bodies = false;
INSERT INTO challenges.challenges (id, name, description, category, designer) VALUES ('0363e243-abd9-4cd0-abeb-b74f0ed6bc3e', 'challenge_1', 'asdf', 'testing', 'asdf');
INSERT INTO challenges.challenges (id, name, description, category, designer) VALUES ('dc103087-3e39-4883-9211-9c58ac1be0d6', 'challenge_2', 'asdf', 'testing', 'asdf');
INSERT INTO challenges.challenges (id, name, description, category, designer) VALUES ('94297dc8-a253-47ff-a7b4-7093b086a296', 'Duality Principal', 'What British mathematician makes these equivalent?
```
!a and !b
```
```
!(a or b)
```
submit as: c2games{First Last}', 'trivia', 'asdf (@asdfasdf)');
INSERT INTO challenges.challenges (id, name, description, category, designer) VALUES ('4e57836e-5dc6-470d-a529-11d4dda41056', 'Hi my name is ascii', 'What does this python program print?
```py
arr = [
    0x48,
    0x65,
    0x6c,
    0x6c,
    0x6f,
    0x20,
    0x77,
    0x6f,
    0x72,
    0x6c,
    0x64,
    0x21
]
msg=""
for i in arr:
    msg = msg + chr(i)
print(msg)
```
Submit as: c2games{output}', 'programming', 'iloveascii');
INSERT INTO challenges.challenges (id, name, description, category, designer) VALUES ('5ca426d8-a2f1-4ed8-9f8c-4adbc9dcec3c', 'K&R Speaks', 'What is the ISBN of the book K&R wrote on an alphabetically named programming language?
Submit as: c2games{123456789}', 'trivia', 'Elegant Avocado');
INSERT INTO challenges.challenges (id, name, description, category, designer) VALUES ('f26059f7-0a3a-4de1-98ff-9450c95127a5', 'what did you call me?', 'What is the exit code of this assembly program?
```txt
          global    _start
          section   .text
_start:   mov       rax, 60
          xor       rdi, rdi
          syscall
```
submit as: c2games{###}', 'programming', 'escapist');
