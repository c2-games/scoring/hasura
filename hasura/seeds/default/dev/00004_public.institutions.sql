SET check_function_bodies = false;
INSERT INTO public.institutions (id, name, img, img_dark, short_name) VALUES ('f1d6a8cd-9a86-4b60-9945-820aefe8184e', 'Tracking API Community College', NULL, NULL, 'TAPICC');
INSERT INTO public.institutions (id, name, img, img_dark, short_name) VALUES ('8cab4f71-ee7f-4940-92d3-5cd35ed0e570', 'Midwest Community College', NULL, NULL, 'MWCC');
INSERT INTO public.institutions (id, name, img, img_dark, short_name) VALUES ('45c53fd3-201c-4071-ab90-86f69a60ff91', 'Skynet University', NULL, NULL, 'TERM');
INSERT INTO public.institutions (id, name, img, img_dark, short_name) VALUES ('784dec1d-568c-4117-85ac-dec079c5d50b', 'Northeast Community College', NULL, NULL, 'NECC');
INSERT INTO public.institutions (id, name, img, img_dark, short_name) VALUES ('4d5470c7-6dd7-435a-893f-22a813d567f2', 'Southeast Community College', NULL, NULL, 'SECC');
INSERT INTO public.institutions (id, name, img, img_dark, short_name) VALUES ('9c24f82a-e6aa-401d-8697-6b544c865fe0', 'Northwest Community College', NULL, NULL, 'NWCC');
INSERT INTO public.institutions (id, name, img, img_dark, short_name) VALUES ('d09add57-daf2-42d9-8a2b-813e07d479f8', 'Southwest Community College', NULL, NULL, 'SWCC');
INSERT INTO public.institutions (id, name, img, img_dark, short_name) VALUES ('4a73edad-5ba7-4c60-8e6a-2f6eb7938d64', 'NCAE CyberGames', NULL, NULL, 'ncae');
