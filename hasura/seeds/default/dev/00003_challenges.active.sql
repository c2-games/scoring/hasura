SET check_function_bodies = false;
INSERT INTO challenges.active (event_id, challenge_id, release_time, points) VALUES ('ef5cf568-e17c-40e1-af87-14b7f8453f82', 'dc103087-3e39-4883-9211-9c58ac1be0d6', '2022-10-14 20:15:00+00', 200);
INSERT INTO challenges.active (event_id, challenge_id, release_time, points) VALUES ('568045b3-6fbe-4f5e-96a3-6db83b5687c0', '94297dc8-a253-47ff-a7b4-7093b086a296', '2023-11-01 00:00:00+00', 100);
INSERT INTO challenges.active (event_id, challenge_id, release_time, points) VALUES ('568045b3-6fbe-4f5e-96a3-6db83b5687c0', '4e57836e-5dc6-470d-a529-11d4dda41056', '2023-11-01 00:00:00+00', 10);
INSERT INTO challenges.active (event_id, challenge_id, release_time, points) VALUES ('568045b3-6fbe-4f5e-96a3-6db83b5687c0', '5ca426d8-a2f1-4ed8-9f8c-4adbc9dcec3c', '2023-11-01 00:00:00+00', 50);
INSERT INTO challenges.active (event_id, challenge_id, release_time, points) VALUES ('568045b3-6fbe-4f5e-96a3-6db83b5687c0', 'f26059f7-0a3a-4de1-98ff-9450c95127a5', '2023-11-01 00:00:00+00', 20);
