SET check_function_bodies = false;
INSERT INTO challenges.tags (challenge_id, tag, scope, color) VALUES ('0363e243-abd9-4cd0-abeb-b74f0ed6bc3e', 'high', 'priority', NULL);
INSERT INTO challenges.tags (challenge_id, tag, scope, color) VALUES ('dc103087-3e39-4883-9211-9c58ac1be0d6', 'medium', 'priority', '#ffff00');
INSERT INTO challenges.tags (challenge_id, tag, scope, color) VALUES ('dc103087-3e39-4883-9211-9c58ac1be0d6', 'low', 'priority', '#aadd00');
