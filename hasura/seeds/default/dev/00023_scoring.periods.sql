SET check_function_bodies = false;
INSERT INTO scoring.periods (id, event_id, start, "end") VALUES ('7634ce69-3e8d-4bc2-97eb-e08ae7bbf8fe', '568045b3-6fbe-4f5e-96a3-6db83b5687c0', '2023-11-01 00:00:00+00', '2023-12-01 02:00:00+00');
INSERT INTO scoring.periods (id, event_id, start, "end") VALUES ('e9867088-7b3f-4cfb-aaa5-d8370a58abae', '568045b3-6fbe-4f5e-96a3-6db83b5687c0', '2023-12-01 03:00:00+00', '2023-12-15 05:00:00+00');
INSERT INTO scoring.periods (id, event_id, start, "end") VALUES ('b2be7f12-a774-49b6-ac2c-7163a420296c', '568045b3-6fbe-4f5e-96a3-6db83b5687c0', '2023-12-15 06:00:00+00', '2023-12-24 08:00:00+00');
INSERT INTO scoring.periods (id, event_id, start, "end") VALUES ('3f5ca4f2-c04a-4461-9df5-060b23c1dd7d', '568045b3-6fbe-4f5e-96a3-6db83b5687c0', '2023-12-26 09:00:00+00', '2024-01-01 11:00:00+00');
INSERT INTO scoring.periods (id, event_id, start, "end") VALUES ('89758908-63b4-459e-b328-b6ef3b2b4b90', '568045b3-6fbe-4f5e-96a3-6db83b5687c0', '2024-01-01 12:00:00+00', '2024-02-01 14:00:00+00');
INSERT INTO scoring.periods (id, event_id, start, "end") VALUES ('66f98f7b-a24e-46d8-b081-2a6380326428', '568045b3-6fbe-4f5e-96a3-6db83b5687c0', '2024-02-01 15:00:00+00', '2024-03-01 17:00:00+00');
INSERT INTO scoring.periods (id, event_id, start, "end") VALUES ('f1a45ada-1df9-4dd5-aed2-88fa5bde20cd', '568045b3-6fbe-4f5e-96a3-6db83b5687c0', '2024-03-01 18:00:00+00', '2024-04-01 20:00:00+00');
INSERT INTO scoring.periods (id, event_id, start, "end") VALUES ('56bc8f72-35cb-4e78-a06f-8af110345181', '568045b3-6fbe-4f5e-96a3-6db83b5687c0', '2024-04-01 21:00:00+00', '2024-05-01 23:00:00+00');
