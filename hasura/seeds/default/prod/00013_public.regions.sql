SET check_function_bodies = false;
INSERT INTO public.regions (id, name) VALUES ('ac333f55-3d80-4048-a685-f7f35a261d6a', 'northwest');
INSERT INTO public.regions (id, name) VALUES ('5bb8d8d1-2a8d-44d0-817a-d1805109536f', 'northeast');
INSERT INTO public.regions (id, name) VALUES ('c0a06f66-0a3e-4d8b-a6be-acd2e304ab30', 'southwest');
INSERT INTO public.regions (id, name) VALUES ('0fb42e39-1145-4341-bc5e-937d944e002c', 'southeast');
INSERT INTO public.regions (id, name) VALUES ('49af36da-1c47-4447-bb9a-d36f05eb045a', 'midwest');
INSERT INTO public.regions (id, name) VALUES ('802dcb0e-9c74-42db-b602-3793047e9ca4', 'east_overflow');
INSERT INTO public.regions (id, name) VALUES ('6db17b15-4f2e-46ad-9349-6e8586861cf7', 'west_overflow');
INSERT INTO public.regions (id, name) VALUES ('63ed4dad-e9ba-404c-9427-876121ef68c9', 'invitational');
