SET check_function_bodies = false;
INSERT INTO public.event_groups (id, name, max_institution_teams_per_event, max_teams_per_event, institution_teams_same_event) VALUES ('0bf6f8ee-f677-4112-88ab-29b42749e9d8', '2023 NCAE CyberGames', 4, 12, false);
INSERT INTO public.event_groups (id, name, max_institution_teams_per_event, max_teams_per_event, institution_teams_same_event) VALUES ('95ca8199-65a9-4ff4-abd1-46dc947ce833', '2023 NCAE CyberGames Finals', 4, 13, false);
INSERT INTO public.event_groups (id, name, max_institution_teams_per_event, max_teams_per_event, institution_teams_same_event) VALUES ('e876414a-5583-4608-b641-64776e5863ee', 'NCAE CyberGames 2024 Regionals', 3, 12, true);
INSERT INTO public.event_groups (id, name, max_institution_teams_per_event, max_teams_per_event, institution_teams_same_event) VALUES ('63d1dfdd-cf33-4ae5-a2c1-e2dcebe57fd0', 'NCAE CyberGames 2024 Invitational', 1, 12, false);
