table:
  name: teams
  schema: public
object_relationships:
  - name: institution
    using:
      foreign_key_constraint_on: institution_id
  - name: vars
    using:
      manual_configuration:
        column_mapping:
          id: team_id
        insertion_order: null
        remote_table:
          name: team
          schema: variables
array_relationships:
  - name: adversarial_statuses
    using:
      foreign_key_constraint_on:
        column: team_id
        table:
          name: adversarial_status
          schema: checks
  - name: awards
    using:
      foreign_key_constraint_on:
        column: team_id
        table:
          name: assertions
          schema: awards
  - name: bonus_points
    using:
      foreign_key_constraint_on:
        column: team_id
        table:
          name: bonus_points
          schema: scoring
  - name: coaches
    using:
      foreign_key_constraint_on:
        column: team_id
        table:
          name: coaches
          schema: public
  - name: deployed_checks
    using:
      foreign_key_constraint_on:
        column: team_id
        table:
          name: deployed
          schema: checks
  - name: event_registration
    using:
      manual_configuration:
        column_mapping:
          id: team_id
        insertion_order: null
        remote_table:
          name: event_registration
          schema: public
  - name: events
    using:
      foreign_key_constraint_on:
        column: team_id
        table:
          name: event_registration
          schema: public
  - name: feedbacks
    using:
      foreign_key_constraint_on:
        column: team_id
        table:
          name: feedback
          schema: wrapup
  - name: members
    using:
      foreign_key_constraint_on:
        column: team_id
        table:
          name: team_membership
          schema: public
  - name: results
    using:
      foreign_key_constraint_on:
        column: team_id
        table:
          name: results
          schema: checks
  - name: service_statuses
    using:
      foreign_key_constraint_on:
        column: team_id
        table:
          name: service_status
          schema: checks
  - name: snapshots
    using:
      foreign_key_constraint_on:
        column: team_id
        table:
          name: snapshots
          schema: wrapup
  - name: snapshots_services
    using:
      foreign_key_constraint_on:
        column: team_id
        table:
          name: snapshots_service
          schema: wrapup
  - name: submissions
    using:
      foreign_key_constraint_on:
        column: team_id
        table:
          name: submissions
          schema: challenges
  - name: team_settings
    using:
      foreign_key_constraint_on:
        column: team_id
        table:
          name: team_settings
          schema: scoreboard
computed_fields:
  - name: max_team_size
    definition:
      function:
        name: max_team_size
        schema: public
  - name: team_full
    definition:
      function:
        name: team_full
        schema: public
  - name: team_size
    definition:
      function:
        name: team_size
        schema: public
remote_relationships:
  - definition:
      to_remote_schema:
        lhs_fields:
          - liaison_id
        remote_field:
          c2games_user_by_pk:
            arguments:
              id: $liaison_id
        remote_schema: keycloak
    name: liaison
  - definition:
      to_remote_schema:
        lhs_fields:
          - team_lead_id
        remote_field:
          c2games_user_by_pk:
            arguments:
              id: $team_lead_id
        remote_schema: keycloak
    name: team_lead
insert_permissions:
  - role: _event_admin
    permission:
      check: {}
      columns:
        - competition_id
        - created
        - id
        - institution_id
        - liaison_id
        - long_name
        - modified
        - name
        - tapi_id
        - team_lead_id
  - role: _event_registrant
    permission:
      check:
        team_lead_id:
          _eq: x-hasura-user-id
      set:
        team_lead_id: x-hasura-user-id
      columns:
        - created
        - id
        - institution_id
        - liaison_id
        - long_name
        - modified
        - name
        - tapi_id
        - team_lead_id
    comment: A custom role here isn't ideal, but allows us to let staff register a regular team while event_admin has unlimited permissions
select_permissions:
  - role: _event_admin
    permission:
      columns:
        - competition_id
        - created
        - id
        - institution_id
        - liaison_id
        - long_name
        - modified
        - name
        - tapi_id
        - team_lead_id
      computed_fields:
        - max_team_size
        - team_full
        - team_size
      filter: {}
      allow_aggregations: true
  - role: _scoringpod
    permission:
      columns:
        - competition_id
        - created
        - id
        - institution_id
        - liaison_id
        - long_name
        - modified
        - name
        - tapi_id
        - team_lead_id
      computed_fields:
        - max_team_size
        - team_full
        - team_size
      filter: {}
      allow_aggregations: true
  - role: _snapshot_service
    permission:
      columns:
        - competition_id
        - tapi_id
        - long_name
        - name
        - created
        - modified
        - id
        - institution_id
        - liaison_id
        - team_lead_id
      computed_fields:
        - max_team_size
        - team_full
        - team_size
      filter: {}
      allow_aggregations: true
    comment: ""
  - role: anonymous
    permission:
      columns:
        - competition_id
        - created
        - id
        - institution_id
        - liaison_id
        - long_name
        - modified
        - name
        - tapi_id
        - team_lead_id
      computed_fields:
        - max_team_size
        - team_full
        - team_size
      filter: {}
      allow_aggregations: true
update_permissions:
  - role: _event_admin
    permission:
      columns:
        - competition_id
        - created
        - id
        - institution_id
        - liaison_id
        - long_name
        - modified
        - name
        - tapi_id
        - team_lead_id
      filter: {}
      check: {}
  - role: _event_registrant
    permission:
      columns:
        - liaison_id
        - long_name
        - modified
        - team_lead_id
      filter:
        team_lead_id:
          _eq: x-hasura-user-id
      check: null
      validate_input:
        definition:
          forward_client_headers: true
          headers: []
          timeout: 10
          url: '{{VALIDATION_URL}}/v1/registration/team/update'
        type: http
    comment: A custom role here isn't ideal, but allows us to let staff register a regular team while event_admin has unlimited permissions
delete_permissions:
  - role: _event_admin
    permission:
      filter: {}
  - role: _event_registrant
    permission:
      filter:
        team_lead_id:
          _eq: x-hasura-user-id
    comment: A custom role here isn't ideal, but allows us to let staff register a regular team while event_admin has unlimited permissions
