#!/bin/bash
set -e

THIS_DIR="$(realpath "$(dirname "$0")")"
HASURA_METADATA_DIR=$(realpath "${THIS_DIR}/../hasura/metadata")
HASURA_ROLES_YML=$(realpath "${HASURA_METADATA_DIR}/inherited_roles.yaml")

ROLES="admin anonymous $(grep role_name: "$HASURA_ROLES_YML" | awk -F': ' '{print $2}' | tr '\n' ' ')"

for role in $ROLES; do
    docker compose exec -T hasura_console bash << EOF
        ENDPOINT="\$(grep "endpoint: " /hasura/config.yaml | cut -d' ' -f 2-)";
        SECRET="\$(grep "admin_secret: " /hasura/config.yaml | cut -d' ' -f 2-)";
        mkdir -p schemas;
        gq "\${ENDPOINT}/v1/graphql" \
            -H "X-Hasura-Admin-Secret: \${SECRET}" \
            -H "X-Hasura-Role: ${role}" \
            --introspect > schemas/$role.graphql
EOF
done
