#!/bin/bash

PG_DUMP="${1:?"Specify postgres dump file"}"

# Export the postgres dump file
docker-compose exec -T hasura_postgres \
    pg_dump -Fc --verbose --clean --no-acl --no-owner \
        -U postgres -d postgres > "$PG_DUMP"

