#!/bin/bash
set -e

THIS_DIR="$(realpath "$(dirname "$0")")"
HASURA_SEED_DIR=$(realpath "${THIS_DIR}/../hasura/seeds/default")
SEED_TARGET="${1:?"Specify one: dev, prod"}"

cd "${HASURA_SEED_DIR}" || exit

IFS=$'\n'
for file in $(find "./${SEED_TARGET}" -type f -name '*.sql' -print | sort); do
    echo "applying ${file}";
    docker compose exec -T hasura_console hasura seed apply \
        --database-name default \
        --file "${file}";
done