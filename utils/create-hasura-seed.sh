#!/bin/bash
set -e

THIS_DIR="$(realpath "$(dirname "$0")")"
HASURA_SEED_DIR=$(realpath "${THIS_DIR}/../hasura/seeds/default")
SEED_TARGET="${1:?"Specify one: dev, prod"}"

# The order of the following tables will impact the ability to restore the data
# tables with no foreign keys should come first and tables that depend on
# other tables due to foreign key relationships should come after
tables="challenges.challenges
public.event_groups
public.events
challenges.active
public.institutions
public.teams
challenges.submissions
challenges.tags
challenges.flags
checks.checks
public.environments
public.coaches
public.event_registration
public.regions
public.event_registration_settings
public.institution_domains
public.institution_region
public.team_membership
public.environment_settings
scoreboard.event_settings
scoreboard.displayed_services
scoreboard.team_settings
scoring.bonus_points
scoring.periods
scoring.points
variables.check
variables.team
variables.deployment
variables.event
checks.deployed
checks.results
wrapup.feedback
wrapup.snapshots
wrapup.snapshots_bonus
wrapup.snapshots_service
awards.badges
awards.assertions
"

# This bash built-in reads tables into MAPFILE
readarray -t <<<"${tables}"

#for table in "${MAPFILE[@]}"; do
for (( i=0; i<${#MAPFILE[@]}; i++ )) ; do
    table="${MAPFILE[${i}]}"
    echo "exporting ${table}"
    output=$(docker-compose exec hasura_console \
        hasura seed create "${SEED_TARGET}_data" \
        --database-name default \
        --from-table "${table}" 2>&1) || exit 1
    filename=$(echo -n "${output}" | jq -r .file)
    # move the file to the seed directory
    # It is important to preserve order as the foreign key relationships
    # impact the order in which data is restored
    mv "${HASURA_SEED_DIR}/${filename##*/}" \
        "${HASURA_SEED_DIR}/${SEED_TARGET}/$(printf "%05d" "${i}")_${table}.sql"
done
