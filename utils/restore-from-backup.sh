#!/bin/bash

THIS_DIR="$(realpath "$(dirname "$0")")"
HASURA_DIR=$(realpath "${THIS_DIR}/../hasura")
HASURA_PARENT_DIR="$(realpath "$(dirname "$HASURA_DIR")")"

METADATA_TAR="${1:?"Specify metadata tarball"}"
PG_DUMP="${2:?"Specify postgres dump file"}"

METADATA_TAR=$(realpath "$METADATA_TAR")
if [ ! -f "$METADATA_TAR" ]; then echo "File does not exist: $METADATA_TAR"; exit 1; fi
PG_DUMP=$(realpath "$PG_DUMP")
if [ ! -f "$PG_DUMP" ]; then echo "File does not exist: $PG_DUMP"; exit 1; fi

if [ -d "$HASURA_DIR" ]; then
    RAND=$(shuf -er -n5  {A..Z} {a..z} {0..9} | tr -d '\n')
    BKP="$HASURA_DIR-$RAND"
    echo "Warning: Hasura directory exists, backup up existing files to $BKP"
    mv "$HASURA_DIR" "$BKP"
fi

tar -xf "$METADATA_TAR" -C "$HASURA_PARENT_DIR"

# Clear any existing metadata
docker-compose exec hasura_console hasura metadata clear

# Restore the postgres dump file
docker-compose exec -T hasura_postgres \
    pg_restore --verbose --clean --no-acl --no-owner \
        -U postgres -d postgres < "$PG_DUMP"

# Apply the metadata in the metadata directory
docker-compose exec hasura_console hasura metadata apply
