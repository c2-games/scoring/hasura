#!/bin/bash

set -eufo pipefail
IFS=$'\n\t'

BACKUP_DIRECTORY="${BACKUP_DIRECTORY:-backups}"
BACKUP_FILE="$BACKUP_DIRECTORY/hasura-routine-backup_$(date -Iseconds).dump"
BACKUP_FILE="${BACKUP_FILE//:/-}"
MAX_AGE=${MAX_AGE:-10}

B2_ACCOUNT_ID="${B2_ACCOUNT_ID:?}"
B2_ACCOUNT_KEY="${B2_ACCOUNT_KEY:?}"
B2_REMOTE_NAME="${B2_REMOTE_NAME:-ncae-hasura}"
B2_BUCKET_NAME="${B2_BUCKET_NAME:-c2-hasura}"

function create_config {
  cat <<EOF
[${B2_REMOTE_NAME}]
type = b2
account = ${B2_ACCOUNT_ID}
key = ${B2_ACCOUNT_KEY}
EOF
}

echo "Creating backup file: ${BACKUP_FILE}"
# Export the postgres dump file
pg_dump -Fc --clean --no-acl --no-owner > "${BACKUP_FILE}"

echo "Uploading to Backblaze"
# create config
config="$(mktemp)"
create_config > "${config}"
rclone copy --config "${config}" "${BACKUP_FILE}" "${B2_REMOTE_NAME}:${B2_BUCKET_NAME}"
rm "${config}"

echo "Backup successfully uploaded: ${BACKUP_FILE}"

