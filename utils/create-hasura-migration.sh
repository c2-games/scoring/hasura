#!/bin/bash

MIGRATION_NAME="init"
DATABASE_NAME="default"

docker-compose exec hasura_console \
    hasura migrate create "${MIGRATION_NAME}" \
        --database-name "${DATABASE_NAME}" \
        --schema public,awards,checks,challenges,scoring,scoreboard,storage,variables,wrapup \
        --from-server
