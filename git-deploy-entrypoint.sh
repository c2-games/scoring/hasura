#!/usr/bin/env bash
set -e
# Test for missing variables
: "${HASURA_GIT_REPO_URL:?}"
# Setup defaults for undefined variables
: "${HASURA_GIT_REFSPEC:="main"}"
: "${HASURA_GIT_PATH:="/hasura"}"
: "${HASURA_GIT_METADATA_DIR:="hasura/metadata"}"
: "${HASURA_GIT_MIGRATIONS_DIR:="hasura/migrations"}"
# Clone git source
mkdir -p "${HASURA_GIT_PATH}"
if [ ! -d "${HASURA_GIT_PATH}/.git" ]; then
    cd /
    git clone --depth=1 "${HASURA_GIT_REPO_URL}" "${HASURA_GIT_PATH}"
fi
cd "${HASURA_GIT_PATH}"
git fetch origin "${HASURA_GIT_REFSPEC}"
git checkout FETCH_HEAD
# Stage hasura metadata dir variable
export HASURA_GRAPHQL_METADATA_DIR="${HASURA_GIT_PATH}/${HASURA_GIT_METADATA_DIR}"
# Stage hasura migrations dir variable
export HASURA_GRAPHQL_MIGRATIONS_DIR="${HASURA_GIT_PATH}/${HASURA_GIT_MIGRATIONS_DIR}"
# Run the original container entrypoint that will do the migrations
# shellcheck disable=SC2068,SC2046,SC2294
/usr/bin/docker-entrypoint-hasura.sh $(eval echo $@)
